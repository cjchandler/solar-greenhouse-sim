import numpy as np
import pandas as pd

from datetime import datetime, date, time
from datetime import timedelta
import matplotlib.dates as mdates
from pytz import timezone
import pytz

import pysolar
import numpy as np

import dateutil.parser



def r_imperial_to_r_si( rimp):
    return rimp/5.67826
#   rsi units are K⋅W−1⋅m2

def r_imperial_to_U_si( rimp):
    d=  rimp/5.67826
    return 1.0/d#
    #U_si units are W⋅m^{-2} K^{-1}

def air_changes_per_hour_to_m3_per_sec( airchangesperhr , volume ):
    return volume*airchangesperhr/(60.0*60.0)

def m3_per_sec_to_air_changes_per_hour( m3_per_sec , volume ):
    return m3_per_sec*(60.0*60.0)/volume

airchangesperhour = 20
vol = 3
m3persec = air_changes_per_hour_to_m3_per_sec(airchangesperhour, vol )
newairchangesperhour = m3_per_sec_to_air_changes_per_hour(m3persec, vol )
assert( np.isclose(m3persec, 0.01666666 , 0.0001 ))
assert( np.isclose(newairchangesperhour, airchangesperhour , 0.0001 ))



def watt_hours_to_mol_PAR( whr ):
    kwhr =  whr*0.001
    return kwhr*7.56 #this is mol PAR per hour

def mol_PAR_to_watt_hours( par ):#par is mol PAR per hour
    kwhr= par/7.56
    return kwhr*1000.0


def spherical_to_cartesian( r , theta , phi ):
    x = np.sin(theta)*np.cos(phi)*r
    y = np.sin(theta)*np.sin(phi)*r
    z = np.cos(theta)*r

    return (x,y,z)

print("spherical_to_cartesian test,  0 ,  0 , 1 : ")
print( spherical_to_cartesian(1.0 , 0 , 0 ) )

def dot_cartesian_vectors(  vec1 , vec2):
    return vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2]

def dot_spherical_vectors( r1 , theta1 , phi1 , r2 , theta2 , phi2):
        return r1*r2*( np.sin(phi1)*np.sin(phi2)*np.cos(theta1-theta2) + np.cos(phi1)*np.cos(phi2) )


def dot_spherical_vectors_noR( theta1, phi1,  theta2, phi2):
    return  ((np.sin(phi1)*np.sin(phi2)*np.cos(theta1 - theta2)) + np.cos(phi1)*np.cos(phi2)   )

def saturation_vapour_pressure( T ):
    # returns in Pa
    exponent = 17.27*T/(T + 237.3)
    return 610.8*np.exp( exponent )

def derivitive_saturation_vapour_pressure( T ):
    # returns in Pa / K
    num = 17.27*T + 17.27*238.3
    dem = (T + 237.3)*(T + 237.3)
    return saturation_vapour_pressure(T)*num/dem


def relative_humidity_to_abs_humidity( T_air_celcius , h_relative ): #h_relative is between 0 and 1

    #this one agrees better with example 7 in Vaisala 2013
    pws = saturation_vapour_pressure(T_air_celcius)
    pw = pws*h_relative

    return 2.166790*pw/(T_air_celcius + 273.15 ) # g m^{-3}


def abs_humidity_to_relative_humidity(  T_air_celcius , h_g_per_m3 ):
    partial_pressure_water = h_g_per_m3*( 273.15+ T_air_celcius)/2.16679
    pws = saturation_vapour_pressure(T_air_celcius)

    return partial_pressure_water/pws

def dewpoint_temperature( T , abs_humidity ):
    # print( "T in dewpoint calcualtion is " + str(T))
    relative_humidity = abs_humidity_to_relative_humidity( T , abs_humidity)
    assert( relative_humidity > 0.0)
    b= 17.67
    c=243.5
    gamma = np.log( relative_humidity) + (b*T)/(c+T)
    # print( ' gamma = ' + str(gamma ) )
    Tdew = (c*gamma)/(b - gamma)
    return Tdew

tt =dewpoint_temperature( 40 , relative_humidity_to_abs_humidity(40 , 0.5)  )
assert( np.isclose( tt , 27.6 , 0.1 ))
assert( np.isclose(  relative_humidity_to_abs_humidity(20 , 0.8 ) ,13.82654470416 , 0.000001  ) )
assert( np.isclose(  abs_humidity_to_relative_humidity(20 , 13.82654470416 ) ,0.8 , 0.000001  ) )

# print( r_imperial_to_r_si(0.68 ) )
# print( r_imperial_to_U_si(0.68 ) )
# print( r_imperial_to_U_si(2 ) )
# print( r_imperial_to_U_si(20 ) )
