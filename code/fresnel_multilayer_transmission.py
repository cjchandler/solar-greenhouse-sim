#########multilayer stuff
import numpy as np
#see notes paper 26 mar 2020

###fresnel equations
def getTheta_t( theta_i, ni , nt):
    return np.arcsin( (ni/nt)*np.sin(theta_i) )

def Rs( theta_i , theta_t , ni , nt):
    num = ni*np.cos(theta_i) - nt*np.cos(theta_t)
    dem = ni*np.cos(theta_i) + nt*np.cos(theta_t)
    return np.abs(num/dem)*np.abs(num/dem)

def Rp( theta_i , theta_t , ni , nt):
    num = ni*np.cos(theta_t) - nt*np.cos(theta_i)
    dem = ni*np.cos(theta_t) + nt*np.cos(theta_i)
    return np.abs(num/dem)*np.abs(num/dem)

def T_single_interface( theta_i , ni, nt ):
    theta_t = getTheta_t( theta_i, ni , nt)
    Tout = 1.0 - 0.5*Rs( theta_i , theta_t , ni , nt  ) - 0.5*Rp( theta_i , theta_t , ni , nt  )

    return Tout

def T_double_interface( theta_i , ni, nt ):
    theta_t = getTheta_t( theta_i, ni , nt)
    t_1 = 1.0 - 0.5*Rs( theta_i , theta_t , ni , nt  ) - 0.5*Rp( theta_i , theta_t , ni , nt  )


    r_1 = 1.0 - t_1
    # t is single interface transmission
    t_2 = t_1 * t_1 / (1.0 - r_1 * r_1)  ##this is transmission through 2 interfaces of index n with multiple reflections included
    return t_2

def T_quadruple_interface( theta_i , ni, nt ): ##assumes that both layers are same material separated with air
    theta_t = getTheta_t(theta_i, ni, nt)

    t_1 = 1.0 - 0.5 * Rs(theta_i, theta_t, ni, nt) - 0.5 * Rp(theta_i, theta_t, ni, nt)
    r_1 = 1.0 - t_1
    # t_1 is single interface transmission
    t_2 = t_1 * t_1 / (1.0 - r_1 * r_1)  ##this is transmission through 2 interfaces of index n with multiple reflections included
    r_2 = 1.0 - t_2

    t_4 = t_2*t_2 / (1.0 - r_2 * r_2)
    return  t_4

def direct_single_interface_T( n ):
    t = 4.0*n/(( n+ 1.0)*(n + 1.0))
    return t


# print( "testing equations against the direct case")
assert( np.isclose( direct_single_interface_T(1.55) ,T_single_interface(0.000001 , 1.0 , 1.55)  , 0.00001 )  )


####for a diffuse light, we need the hemispherical transmission. Now technically the mirror and albedo could make this more complicated, but we'll leave it a constant for now (7 april 2020)


class T_1interface_integrand(object):
    def __init__(self, n_film):
        self.n_index = n_film

    def __call__(self, theta_coordinates , phi_coordinates ):
        return T_single_interface(theta_coordinates, 1.0 ,  self.n_index) * np.cos(
            theta_coordinates) * np.sin(theta_coordinates)  # 2016 hemming powerpoint for this equation.n

class T_2interface_integrand(object):
    def __init__(self, n_film):
        self.n_index = n_film

    def __call__(self, theta_coordinates , phi_coordinates ):
        return T_double_interface(theta_coordinates, 1.0 ,  self.n_index) * np.cos(
            theta_coordinates) * np.sin(theta_coordinates)  # 2016 hemming powerpoint for this equation.n

class T_4interface_integrand(object):
    def __init__(self, n_film):
        self.n_index = n_film

    def __call__(self, theta_coordinates , phi_coordinates ):
        return T_quadruple_interface(theta_coordinates, 1.0 ,  self.n_index) * np.cos(
            theta_coordinates) * np.sin(theta_coordinates)  # 2016 hemming powerpoint for this equation.n


###these are old testing functions
def f( theta , phi):
    return np.sin(theta)*np.cos(theta)


def Told_integrand( theta_coordinates , phi_theta_coordinates):
    ni = 1.0
    nt = 1.55
    theta_i = theta_coordinates
    theta_t = getTheta_t( theta_i , ni , nt)

    R = 0.5*Rs( theta_i , theta_t , ni , nt) + 0.5*Rp( theta_i , theta_t , ni , nt)

    return (1.0 - R)*np.sin(theta_i)*np.cos(theta_i) #2016 hemming powerpoint for this equation.

from scipy import integrate



totaltransmission =  integrate.dblquad(f, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)  # this is pi
tcalcualted = integrate.dblquad(Told_integrand, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)
# print( tcalcualted[0])
# print(totaltransmission[0])



t1func = T_1interface_integrand(1.55)
t_1interface_calcualted = integrate.dblquad(t1func, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)
print( t_1interface_calcualted[0])

assert( np.isclose(t_1interface_calcualted[0],tcalcualted[0], 0.000001 )  )

# t_2interface_calcualted = integrate.dblquad(T_2interface_integrand, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)
# t_4interface_calcualted = integrate.dblquad(T_4interface_integrand, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)
t4func = T_4interface_integrand( 1.55)
t_4interface_calcualted = integrate.dblquad(t4func, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)
# print(t_4interface_calcualted[0]/np.pi )

def hemispherical_4interface_transmission( n_film):
    t4func = T_4interface_integrand(n_film)
    t_4interface_calcualted = integrate.dblquad(t4func, 0, 2.0 * np.pi, lambda x: 0, lambda x: 0.5 * np.pi)
    return(t_4interface_calcualted[0] / np.pi)


t4func = T_4interface_integrand( 1.55)
t_4interface_calcualted = integrate.dblquad(t4func, 0, 2.0*np.pi , lambda x: 0, lambda x: 0.5*np.pi)
# print(t_4interface_calcualted[0]/np.pi )

def hemispherical_2interface_transmission( n_film):
    t2func = T_2interface_integrand(n_film)
    t_2interface_calcualted = integrate.dblquad(t2func, 0, 2.0 * np.pi, lambda x: 0, lambda x: 0.5 * np.pi)
    return(t_2interface_calcualted[0] / np.pi)


ntest = 1.4
print( "direct single film T , n = " + str(ntest) )
print( T_double_interface( 0 , 1.0, ntest ) )
print( "diffuse single film T n = " + str(ntest) )
print( hemispherical_2interface_transmission(ntest) )
