#This is the greenouse structure where all the panels are used together to model interior enviroment
import conversion_funcs as converting
from panel import *
from scipy import optimize

class greenhouse:
    def __init__(self ):
        self.panel_list = []
        self.volume = 1.0 #m3
        self.floor_area = 1.0 #m2 this is area used for growing plants

        self.air_topsoil_conductivity = 2.0 # W·m^-2 K^−1.
        self.topsoil_deepsoil_conductivity = 2.0 # W·m^-2 K^−1
        self.infiltration_air_changes_per_hour = 0.25 #air exchange when all fans off, openings closed
        self.topsoil_specific_heat = 1400000.0# J/(K )  so if the topsoil is really just water, then this is kgwater*4182
        #use total kg of water or topsoil in the whole greenhouse

        #thermostat set points
        self.day_air_max = 30 # all in degrees c
        self.day_air_min = 20
        self.night_air_max = 30
        self.night_air_min = 10

    def print( self):
        print('-------Greenhouse has params: -------------------')
        print('    volume m3 :' + str(self.volume) )
        print('    floor_area m2 :' + str(self.floor_area) )
        print('    air_topsoil_conductivity  W·m^-2 K^−1 :' + str(self.air_topsoil_conductivity) )
        print('    topsoil_deepsoil_conductivity  W·m^-2 K^−1 :' + str(self.topsoil_deepsoil_conductivity) )
        print('    topsoil_specific_heat  J K^{-1}  :' + str(self.topsoil_specific_heat) )
        return 0

class greenhouse_state:
    def __init__(self ):
        self.crop_area = 1.0 #this is a scaling parameter for transpiration rate
        self.isday = True
        self.panel_state_list = []
        self.humidity_in  = 0#abs humidity inside greenhouse, g m^{-3}
        self.T_in = 0
        self.T_out = 0
        self.T_sky = 0
        self.T_topsoil = 0
        self.T_deepsoil = 13.0 ##deep soil temperature is a year round constant
        self.Q_forcing  = 0 #this is heating or cooling, in watts for the whole
        #greenhouse as required to keep energy balance constant. If the greenhouse is
        #in a steady state, not heating up or cooling Q_forcing = 0
        self.W_forcing  = 0 #this is adding (+) or removing humidity , in g water/s
        #for whole greenhouse needed from either a mist system or dehumdifer for
        #keep moisture balance constant. If humidty is constante, W_forcing = 0

        self.forced_air_changes_per_hour = 0.25 #this is set to do air changes for cooling that is, when Q_forcing is negative. #this has to agree with Q
        self.light_in_per_m2 = 0 #this is total light transmitted into greenhouse divided by floor area m2 in watts
        self.PAR_in_per_m2 = 0 #same as light but as mol PAR m^{-2} hr^{-1}

    def energy_loss_single_panel_conduction( self , panelA, panel_stateA):
        deltaT = (self.T_out - self.T_in)
        conductivity = 0
        conductivity = panel_stateA.U
        return deltaT*panelA.area*conductivity

    def print( self ):
        print( 'greenhouse state: -------------------')
        print( '  relative humidity in ' + str( converting.abs_humidity_to_relative_humidity( self.T_in , self.humidity_in) ) )
        print( '  temperature in ' + str( self.T_in ) )
        print( '  light watts / m2 ' + str( self.light_in_per_m2 ) )
        print( '  forced_air_changes_per_hour ' + str( self.forced_air_changes_per_hour ) )
        print( '  top soil temp ' + str( self.T_topsoil) )
        print( '  deep soil temp ' + str( self.T_deepsoil) )
        u = []
        for p in self.panel_state_list:
            u.append( p.U)

        print( '  mean U of glazing ' + str( np.mean(u) ) )




def sky_temperature( weather_stateA):
    #Mark A. Goforth, George W. Gilchrist, and Joseph D. Sirianni, "Cloud effects on thermal downwelling sky radiance," AeroSense 2002, International Society for Optics and Photonics (2002).
    #https://physics.stackexchange.com/questions/153839/what-is-the-temperature-of-the-clear-night-sky-from-the-surface-of-earth
    if weather_stateA.RelativeHumidity == 0.0: #equation doesn't work at 0 realtive humidity, this makes sure that never happens. 0 to 5 isn't a big chance and shouldn't induce too much error.
        weather_stateA.RelativeHumidity = 5.0
    assert(  weather_stateA.RelativeHumidity > 1.1 ) #this is making sure that the humidity was recorded as a percentage, as is needed for following function
    powersky = (1.0 + 0.34*weather_stateA.CloudOpacity*0.01*weather_stateA.CloudOpacity*0.01 )*8.78e-13*np.power( weather_stateA.AirTemp+273 , 5.852 )*np.power(weather_stateA.RelativeHumidity,0.07195)
    tempsky = np.power( (powersky/(0.74*5.67e-8)) , 0.25)
    skytemp = tempsky-273.0
    assert( skytemp > -100)
    assert( skytemp < 60)

    return skytemp

def make_greenhouse_state( greenhouseA ,  weather_stateA):
    greenhouse_stateA = greenhouse_state()
    greenhouse_stateA.T_sky = sky_temperature(weather_stateA)
    greenhouse_stateA.T_out = weather_stateA.AirTemp
    greenhouse_stateA.humidity_in = converting.relative_humidity_to_abs_humidity( weather_stateA.AirTemp, weather_stateA.RelativeHumidity*0.01)
    greenhouse_stateA.T_in = weather_stateA.AirTemp

    greenhouse_stateA.isday = True
    if( weather_stateA.Ghi < 10 ):
        greenhouse_stateA.isday = False

    light_in = 0.0
    for p in greenhouseA.panel_list:
        if greenhouse_stateA.isday == True:
            ps= panel_state()
            ps.U = p.U_value_without_blanket
            ps = simulate_panel( p , ps ,weather_stateA)
            light_in = light_in + ps.total_diffuse_in + ps.total_direct_in
            ps = remove_blanket(p,ps)
            greenhouse_stateA.panel_state_list.append(ps)

        if greenhouse_stateA.isday == False:
            ps= panel_state()
            ps.U = p.U_value_with_blanket
            ps = deploy_blanket(p,ps)
            greenhouse_stateA.panel_state_list.append(ps)


    greenhouse_stateA.light_in_per_m2 = light_in/greenhouseA.floor_area
    greenhouse_stateA.PAR_in_per_m2 = converting.watt_hours_to_mol_PAR( greenhouse_stateA.light_in_per_m2 )
    return greenhouse_stateA



def psychrometric_constant(weather_stateA):
     return 72.08*(weather_stateA.SurfacePressure/1013.8)



def transpiration_humidity_change_rate( greenhouseA , greenhouse_stateA , weather_stateA):
    #returns g water vapour produced per second
    #Katsoulas2019 paper for formula
    num = converting.derivitive_saturation_vapour_pressure(greenhouse_stateA.T_in)*greenhouse_stateA.light_in_per_m2
    # print( num)
    dem = converting.derivitive_saturation_vapour_pressure(greenhouse_stateA.T_in) + psychrometric_constant(weather_stateA)
    # print( dem)
    return 1000.0*greenhouse_stateA.crop_area*(num/dem)/2260000.0

    #Is this good enough? I don't know until we experimentally validate it. That
    #said, this function has access to all the information I have, so extending
    #the model shouldn't wreck anything else

def condensation_humidity_change_rate( greenhouseA , greenhouse_stateA , weather_stateA):
    #g water condensed per sec for all the greenhouse panels

    #for each panel calcualte the panel film_temperature
    for a in range( 0 , len(greenhouse_stateA.panel_state_list) ):
        greenhouse_stateA.panel_state_list[a].panel_film_temperature = panel_film_temperature(greenhouseA.panel_list[a], greenhouse_stateA.T_in , weather_stateA.AirTemp )
        # print( 'panel film temperature = ' + str(greenhouse_stateA.panel_state_list[a].panel_film_temperature))

    #calculate inside dewpoint
    T_dew =converting.dewpoint_temperature( greenhouse_stateA.T_in , greenhouse_stateA.humidity_in )
    # print( 'dew point temp is '+ str(T_dew))

    #for panels with film_temperature below dewpoint, calculate heat loss via conduction
    #convert heat loss via conduction into amount of water vapour condensed and cooled to film_temperature
    water_condensed_kg = 0.0
    for a in range( 0 , len(greenhouse_stateA.panel_state_list) ):
        if greenhouse_stateA.panel_state_list[a].panel_film_temperature <= T_dew:
            if( T_dew < 0.0  ):
                return 0.0 #The formulas breakdown if there is a frost coating on panel
            Econductionlostwatts = -1.0*greenhouse_stateA.energy_loss_single_panel_conduction( greenhouseA.panel_list[a], greenhouse_stateA.panel_state_list[a])
            # print( 'panel Energy lost is watts = ' +str(Econductionlostwatts))
            water_condensed_kg = water_condensed_kg + Econductionlostwatts/2260000.0

    return -1.0*water_condensed_kg*1000.0

def venting_humidity_change_rate( greenhouseA , greenhouse_stateA , weather_stateA):
    #negative  means g water lost from greenhouse per second
    abs_humidity_out = converting.relative_humidity_to_abs_humidity( weather_stateA.AirTemp , weather_stateA.RelativeHumidity*0.01 )
    abs_humidity_in = greenhouse_stateA.humidity_in
    # print( 'abs humidity out is '+ str(abs_humidity_out))
    # print( 'abs humidity in is '+ str(abs_humidity_in) )
    m3_air_per_sec = greenhouseA.volume*( greenhouse_stateA.forced_air_changes_per_hour  + greenhouseA.infiltration_air_changes_per_hour)/(60.0*60.0 )
    # print( '--- m3_air_per_sec    ' + str(m3_air_per_sec) )
    # print( '--- abs_humidity_out    ' + str(abs_humidity_out) )
    # print( '--- abs_humidity_in    ' + str(abs_humidity_in) )
    return m3_air_per_sec*( abs_humidity_out - abs_humidity_in  )




#passsive because this is the air that is exchanged with ouside due to small holes in the greenhouse and in uncontrolalbe
def passive_venting_energy_change(  greenhouseA , greenhouse_stateA , weather_stateA ): #this is in watts
        #negative  means g water lost from greenhouse per second
    abs_humidity_out = converting.relative_humidity_to_abs_humidity( weather_stateA.AirTemp , weather_stateA.RelativeHumidity*0.01 )
    abs_humidity_in = greenhouse_stateA.humidity_in
        # print( 'abs humidity out is '+ str(abs_humidity_out))
        # print( 'abs humidity in is '+ str(abs_humidity_in) )
    m3_air_per_sec = greenhouseA.volume*( greenhouseA.infiltration_air_changes_per_hour  )/(60*60.0)

    latent_heat_watts = 2260000*m3_air_per_sec*( abs_humidity_out*0.001 - abs_humidity_in*0.001  ) #0.001 because we measure abs humidity in g/m3 and this used kg water latent heat

    deltaT = (weather_stateA.AirTemp - greenhouse_stateA.T_in)
    specific_heat_watts = 0.355*deltaT*( greenhouseA.infiltration_air_changes_per_hour  )*greenhouseA.volume

    return specific_heat_watts + latent_heat_watts


def forced_venting_energy_change(  greenhouseA , greenhouse_stateA , weather_stateA ): #this is in watts
        #negative  means g water lost from greenhouse per second
    abs_humidity_out = converting.relative_humidity_to_abs_humidity( weather_stateA.AirTemp , weather_stateA.RelativeHumidity*0.01 )
    abs_humidity_in = greenhouse_stateA.humidity_in
        # print( 'abs humidity out is '+ str(abs_humidity_out))
        # print( 'abs humidity in is '+ str(abs_humidity_in) )
    m3_air_per_sec = greenhouseA.volume*( greenhouse_stateA.forced_air_changes_per_hour  )/(60*60.0)
    latent_heat_watts = 2260000*m3_air_per_sec*( abs_humidity_out*0.001 - abs_humidity_in*0.001  ) #0.001 because we measure abs humidity in g/m3 and this used kg water latent heat
    # print('        latent heat : ' + str(latent_heat_watts))


    deltaT = (weather_stateA.AirTemp - greenhouse_stateA.T_in)
    specific_heat_watts = 0.355*deltaT*( greenhouse_stateA.forced_air_changes_per_hour  )*greenhouseA.volume
    # print('        specific_heat_watts : ' + str(specific_heat_watts))

    return specific_heat_watts + latent_heat_watts



def air_soil_conduction_energy_change( greenhouseA , greenhouse_stateA , weather_stateA): #energy flow from air to soil is negative
    deltaT = (greenhouse_stateA.T_topsoil - greenhouse_stateA.T_in)
    return deltaT*greenhouseA.floor_area*greenhouseA.air_topsoil_conductivity

def topsoil_deepsoil_conduction_energy_change( greenhouseA , greenhouse_stateA , weather_stateA): #energy flow from air to soil is negative
    deltaT = (-greenhouse_stateA.T_topsoil + greenhouse_stateA.T_deepsoil)
    return deltaT*greenhouseA.floor_area*greenhouseA.topsoil_deepsoil_conductivity

def glazing_conduction_energy_change( greenhouseA , greenhouse_stateA , weather_stateA):
    deltaT = (greenhouse_stateA.T_out - greenhouse_stateA.T_in)
    E = 0
    for a in range( 0 , len(greenhouseA.panel_list)):
        E = E + deltaT*greenhouseA.panel_list[a].area*greenhouse_stateA.panel_state_list[a].U

    return E

def glazing_radiation_energy_change( greenhouseA , greenhouse_stateA , weather_stateA):
    tdiffsky = ( -np.power(greenhouse_stateA.T_in+273, 4) + np.power(greenhouse_stateA.T_sky+273, 4)  )
    stephanboltzmanconstant = 5.67e-8
    eA = 0
    for a in range( 0 , len(greenhouseA.panel_list)):
        eA = eA + greenhouse_stateA.panel_state_list[a].e_value*greenhouseA.panel_list[a].area

    out= eA*stephanboltzmanconstant*( tdiffsky )
    return out

def solar_radiation_energy_change( greenhouseA , greenhouse_stateA , weather_stateA):
    return greenhouse_stateA.light_in_per_m2*greenhouseA.floor_area


def top_soil_energy_change( greenhouseA , greenhouse_stateA , weather_stateA):
    # + is net energy into topsoil, - is net energy going out of topsoil in watts
    watts = (-air_soil_conduction_energy_change( greenhouseA , greenhouse_stateA , weather_stateA)
        +topsoil_deepsoil_conduction_energy_change( greenhouseA , greenhouse_stateA , weather_stateA) )
    return watts

def top_soil_temperature_change( greenhouseA , greenhouse_stateA , weather_stateA , period_seconds):
    watts = top_soil_energy_change( greenhouseA , greenhouse_stateA , weather_stateA)
    joules = watts*period_seconds
    deltaT = joules/greenhouseA.topsoil_specific_heat

    return deltaT


def find_energy_and_humidity_forcing( greenhouseA , greenhouse_stateA , weather_stateA):
#this takes a state and returns the needed Q (+/- heat flux) and W (+/- water vapour) to achive steady state
# + Q is greenhouse is heating up, - Q greenhouse is cooling, watts
# +W greenhouse is becoming more humid, -W humidity is decreasing, in g water/sec

#a state with Q =W=0 is steady state

    radiation = glazing_radiation_energy_change( greenhouseA , greenhouse_stateA , weather_stateA)
    conduction = glazing_conduction_energy_change( greenhouseA , greenhouse_stateA, weather_stateA)
    passiveventing=passive_venting_energy_change(  greenhouseA , greenhouse_stateA, weather_stateA )
    forcedventing= forced_venting_energy_change(greenhouseA , greenhouse_stateA, weather_stateA)
    sun = solar_radiation_energy_change( greenhouseA , greenhouse_stateA, weather_stateA)
    #
    # print('    -energy balance: ')
    # print('     radiation = ' + str( radiation) )
    # print('     conduction = ' + str( conduction) )
    # print('     passiveventing = ' + str( passiveventing) )
    # print('     forcedventing = ' + str( forcedventing) )
    # print('     sun = ' + str( sun) )

    Q = (glazing_radiation_energy_change( greenhouseA , greenhouse_stateA , weather_stateA)
        + glazing_conduction_energy_change( greenhouseA , greenhouse_stateA, weather_stateA)
        + topsoil_deepsoil_conduction_energy_change( greenhouseA , greenhouse_stateA, weather_stateA)
        + air_soil_conduction_energy_change( greenhouseA , greenhouse_stateA, weather_stateA)
        + passive_venting_energy_change(  greenhouseA , greenhouse_stateA, weather_stateA )
        + solar_radiation_energy_change( greenhouseA , greenhouse_stateA, weather_stateA)
        +forced_venting_energy_change(greenhouseA , greenhouse_stateA, weather_stateA)
    )

    W = (venting_humidity_change_rate( greenhouseA , greenhouse_stateA, weather_stateA)
        + condensation_humidity_change_rate( greenhouseA , greenhouse_stateA, weather_stateA)
        + transpiration_humidity_change_rate( greenhouseA , greenhouse_stateA, weather_stateA)
    )
    return Q,W

#export date from a liste of greenhouse states to pandas for plotting and analysis
def greenhouse_state_list_to_df( greenhouse_state_listA , weather_df):
    n_data = len(greenhouse_state_listA)

    crop_area = np.zeros(n_data) #this is a scaling parameter for transpiration rate
    isday = np.zeros(n_data)
    # self.panel_state_list = [] #not sure how to deal this this... Is it of final interest?
    humidity_in  = np.zeros(n_data)#abs humidity inside greenhouse, g m^{-3}
    relative_humidity_in  = np.zeros(n_data)#abs humidity inside greenhouse, g m^{-3}
    T_in = np.zeros(n_data)
    T_out = np.zeros(n_data)
    T_sky = np.zeros(n_data)
    T_topsoil = np.zeros(n_data)
    T_deepsoil = np.zeros(n_data) ##deep soil temperature is a year round constant
    W_forcing  = np.zeros(n_data)
    Q_forcing  = np.zeros(n_data) #this is heating or cooling, in watts for the whole
    #greenhouse as required to maintaing the thermostat set points .
    forced_air_changes_per_hour = np.zeros(n_data) #this is set to do air changes for cooling that is, when Q_forcing is negative. #this has to agree with Q
    light_in_per_m2 = np.zeros(n_data) #this is total light transmitted into greenhouse divided by floor area m2 in watts
    PAR_in_per_m2 = np.zeros(n_data) #same as light but as mol PAR m^{-2} hr^{-1}

    for a in range( 0 , n_data):
        crop_area[a] = greenhouse_state_listA[a].crop_area
        isday[a] = greenhouse_state_listA[a].isday
        humidity_in[a] = greenhouse_state_listA[a].humidity_in
        relative_humidity_in[a] = converting.abs_humidity_to_relative_humidity(greenhouse_state_listA[a].T_in, greenhouse_state_listA[a].humidity_in )
        T_in[a] = greenhouse_state_listA[a].T_in
        T_out[a] = greenhouse_state_listA[a].T_out
        T_sky[a] = greenhouse_state_listA[a].T_sky
        T_topsoil[a] = greenhouse_state_listA[a].T_topsoil
        T_deepsoil[a] = greenhouse_state_listA[a].T_deepsoil
        Q_forcing[a] = greenhouse_state_listA[a].Q_forcing
        W_forcing[a] = greenhouse_state_listA[a].W_forcing
        forced_air_changes_per_hour[a] = greenhouse_state_listA[a].forced_air_changes_per_hour
        light_in_per_m2[a] = greenhouse_state_listA[a].light_in_per_m2
        PAR_in_per_m2[a] = converting.watt_hours_to_mol_PAR( greenhouse_state_listA[a].light_in_per_m2)

    dfout = pd.DataFrame(index=weather_df.index)
    dfout['crop_area'] =crop_area
    dfout['isday'] = isday
    dfout['humidity_in'] = humidity_in
    dfout['relative_humidity_in'] = relative_humidity_in
    dfout['T_in'] = T_in
    dfout['T_out'] = T_out
    dfout['T_sky'] = T_sky
    dfout['T_topsoil'] = T_topsoil
    dfout['T_deepsoil'] = T_deepsoil
    dfout['Q_forcing'] = Q_forcing
    dfout['W_forcing'] = W_forcing
    dfout['forced_air_changes_per_hour'] = forced_air_changes_per_hour
    dfout['light_in_per_m2'] = light_in_per_m2
    dfout['PAR_in_per_m2'] = PAR_in_per_m2

    return dfout
