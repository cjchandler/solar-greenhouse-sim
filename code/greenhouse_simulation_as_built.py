#get some weather data, make a list of weather_state objects
from weather_data import *
import numpy as np
from panel import *
from greenhouse import *
from greenhouse_solver import *
import conversion_funcs as converting
import pandas as pd
import matplotlib.pyplot as plt

path = '../test_solcast_data/'
filename = 'testmonth_Solcast_PT60M.csv'
f = load_solcast_historical_df( path , filename )
print(f)

# df_reindexed = f.reindex(pd.date_range(start=f.index.min(),end=f.index.max(),freq='30T'))
# f = df_reindexed.interpolate(method='linear', limit_direction='forward', axis=0)
# print(  f )


weather_state_list = make_weather_state_list(f)

#define some panels

p_roof = panel()
p_wall = panel()
p_rest = panel()


p_wall.theta = np.pi/2.0
p_wall.phi = np.pi
p_wall.sky_fraction = 0.5
p_wall.ground_fraction = 0.5
p_wall.area = 16*6*0.092903

p_roof.theta = np.deg2rad(60.0 )
p_roof.phi = np.pi #south
p_roof.sky_fraction = 0.8333
p_roof.ground_fraction = 0.1666
p_roof.area = 16*6.7*0.092903

p_roof.n_index = 1.4
p_wall.n_index = 1.4
p_roof.U_value_with_blanket= converting.r_imperial_to_U_si( 20.0 )
p_roof.U_value_without_blanket = converting.r_imperial_to_U_si( 2.0 )
p_wall.U_value_with_blanket= converting.r_imperial_to_U_si( 20.0 )
p_wall.U_value_without_blanket = converting.r_imperial_to_U_si( 2.0 )

p_rest.area = (72 + 18 +72+18 + (16*6) + (16*6.7) )*0.092903 #m2 of non glazed p_wall
p_rest.n_index = 1000000.0
p_roof.U_value_with_blanket= converting.r_imperial_to_U_si( 20.0 )
p_roof.U_value_without_blanket = converting.r_imperial_to_U_si( 20.0 )

panel_list = []



panel_list.append(p_wall)
panel_list.append(p_roof)
panel_list.append(p_rest)




#simulate those panels for light transmission
p_roof_state_list = simulate_panel_for_panel_state_list( p_roof , weather_state_list)
p_wall_state_list = simulate_panel_for_panel_state_list( p_wall , weather_state_list)
p_rest_state_list = simulate_panel_for_panel_state_list( p_rest , weather_state_list)

weather_state66 = weather_state_list[66]

#add panels to greenhouse object
greenhouseA = greenhouse()
greenhouseA.floor_area = 16*6*0.092903
greenhouseA.volume = (16*12*6+ 16+12*1.5 )*0.0283168
greenhouseA.topsoil_specific_heat = 4182.0*1000*2.0*greenhouseA.floor_area*0.2
greenhouseA.panel_list = [p_roof, p_wall , p_rest]
greenhouse_state_test = make_greenhouse_state(greenhouseA , weather_state66)



best_state, bestabsq = brute_force_solver( greenhouseA , weather_state66)
best_state.print()
print( "best q =  "+ str( bestabsq))

print( ' U for r-2 is '+ str(converting.r_imperial_to_U_si( 2.0 ) ) )
print( ' U for r-20 is '+ str(converting.r_imperial_to_U_si( 20.0 ) ) )



print( ' -----------------------winter solver and thermal mass testing-----------------------------------')
out = winter_solver( greenhouseA , weather_state66)
out.print()
greenhouseA.print()
out2 = winter_solver_enumerate_thermal_mass(greenhouseA ,out,weather_state66 , 60*60 )
out2.print()

deltaTopsoil = top_soil_temperature_change(greenhouseA , out, weather_state66, 60*60)
print( "calc topsoil delta = " + str(deltaTopsoil) )
#assert( np.isclose( deltaTopsoil, 0.1347 , 0.001 ))




print( '------------------------making a data set for plotting----------------------------------------------------------')
n_data = len(weather_state_list)
# n_data = 100
greenhouse_state_list= []
out.T_topsoil = 11
greenhouse_state_list.append(out)
for a in range( 1 , n_data):
    state = winter_solver_enumerate_thermal_mass(greenhouseA ,greenhouse_state_list[a-1],weather_state_list[a] , 60*60 )
    if( np.abs(state.Q_forcing) > 0.01 ):
        state = winter_solver_enumerate_thermal_mass(greenhouseA ,greenhouse_state_list[a-1],weather_state_list[a] , 60*60 )

    greenhouse_state_list.append( state )

greenhouse_state_df = greenhouse_state_list_to_df(greenhouse_state_list, f )
greenhouse_state_df.to_csv( 'test_month_asbuilt_greenhouse_state_df.csv')


g = pd.read_csv('test_month_asbuilt_greenhouse_state_df.csv', index_col='PeriodStart', parse_dates=True, sep=',')
g[[ 'T_in' , 'T_out' , 'isday' , 'Q_forcing' , 'W_forcing' , 'T_topsoil' ,'forced_air_changes_per_hour' , 'light_in_per_m2']].plot(grid=True )
plt.show()
