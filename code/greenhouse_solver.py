#this is methods for simulating the greenhouse to find a stationary state, or physically possible state

#get some weather data.

#setup a greenhouse

#use the solver functions here to find allowed inside temperature and humidity.



from weather_data import *
import numpy as np
import scipy
from panel import *
from greenhouse import *
import copy

def winter_solver(greenhouseA ,  weather_stateA):
    #give a greenhouse, find the T_in that uses no heating or cooling. humidity_in fixed. Aims for highest temperature possible, hence winter_solver
    global_RH_inside = 0.9

    greenhouse_state_orig = make_greenhouse_state( greenhouseA ,  weather_stateA)
    # print( "starting state was : ")
    # greenhouse_state_orig.print()

    temperature_list = np.arange( weather_stateA.AirTemp , 40 , 0.5 )
    q_list = np.zeros( len(temperature_list) )
    trial_greenhouse_state_list = []
    for a in range( 0 , len(temperature_list) ):
        T = temperature_list[a]
        trial_greenhouse_state = greenhouse_state()
        trial_greenhouse_state = greenhouse_state_orig
        #inside abs humidity is
        trial_greenhouse_state.humidity_in = converting.relative_humidity_to_abs_humidity( T , global_RH_inside)
        abs_humidity_out = converting.relative_humidity_to_abs_humidity( weather_stateA.AirTemp ,weather_stateA.RelativeHumidity*0.01 )
        trial_greenhouse_state.T_in = T
        trial_greenhouse_state.forced_air_changes_per_hour = 0.0

        transpiration_rate = transpiration_humidity_change_rate( greenhouseA , trial_greenhouse_state, weather_stateA)
        condensation_rate = condensation_humidity_change_rate( greenhouseA , trial_greenhouse_state, weather_stateA)
        delta_abs_humidity = abs_humidity_out -trial_greenhouse_state.humidity_in
        venting_m3_per_sec = -1.0*(transpiration_rate + condensation_rate )/delta_abs_humidity
        airchanges_per_hr = converting.m3_per_sec_to_air_changes_per_hour(venting_m3_per_sec, greenhouseA.volume )
        if(airchanges_per_hr >=  greenhouseA.infiltration_air_changes_per_hour):
            trial_greenhouse_state.forced_air_changes_per_hour = airchanges_per_hr - greenhouseA.infiltration_air_changes_per_hour
        else:
            trial_greenhouse_state.forced_air_changes_per_hour  = 0.0


        #now the humidity change is constant, or resolvable
        q,w = find_energy_and_humidity_forcing( greenhouseA , trial_greenhouse_state , weather_stateA)
        # print( T, trial_greenhouse_state.forced_air_changes_per_hour  , q,w)
        # if( np.isclose(w , 0.0 , 0.00001 ) == False):
        #     q = 1000000 #this is a flag that the moisture is not steady state
        trial_greenhouse_state.Q_forcing = q
        trial_greenhouse_state.W_forcing = w

        q_list[a] = q
        trial_greenhouse_state_list.append( copy.deepcopy(trial_greenhouse_state) )

    greenhouse_state_optimum = greenhouse_state()
    bestabsq = 100000
    for a in range( 0 , len(temperature_list) ):
        if( abs(q_list[a]) < bestabsq):
            bestabsq = abs(q_list[a])
            # print( bestabsq)
            greenhouse_state_optimum = trial_greenhouse_state_list[a]

        # greenhouse_state_optimum.print()

    return greenhouse_state_optimum


class winter_solver_functor:
    def __init__(self , greenhouseA , greenhouse_stateA , previous_greenhouse_stateA, weather_stateA, global_RH,  period_secs):
        self.greenhouse = greenhouseA
        self.greenhouse_state = greenhouse_stateA
        self.previous_greenhouse_state = previous_greenhouse_stateA
        self.weather_state = weather_stateA
        self.global_RH_inside = global_RH
        self.period_seconds = period_secs

    def __call__(self, T):
        #return q an indication of how close the state is to steady temperature
        trial_greenhouse_state = greenhouse_state()
        trial_greenhouse_state = self.greenhouse_state
        trial_greenhouse_state.T_topsoil = self.previous_greenhouse_state.T_topsoil
        #inside abs humidity is
        trial_greenhouse_state.humidity_in = converting.relative_humidity_to_abs_humidity( T , self.global_RH_inside)
        abs_humidity_out = converting.relative_humidity_to_abs_humidity( self.weather_state.AirTemp , self.weather_state.RelativeHumidity*0.01 )
        trial_greenhouse_state.T_in = T
        trial_greenhouse_state.forced_air_changes_per_hour = 0.0

        transpiration_rate = transpiration_humidity_change_rate( self.greenhouse , trial_greenhouse_state, self.weather_state )
        condensation_rate = condensation_humidity_change_rate( self.greenhouse , trial_greenhouse_state, self.weather_state )
        delta_abs_humidity = abs_humidity_out -trial_greenhouse_state.humidity_in
        venting_m3_per_sec = -1.0*(transpiration_rate + condensation_rate )/delta_abs_humidity
        airchanges_per_hr = converting.m3_per_sec_to_air_changes_per_hour(venting_m3_per_sec, self.greenhouse.volume )
        # if( airchanges_per_hr >= 120 ):
        #     airchanges_per_hr = 120
        if(airchanges_per_hr >=  self.greenhouse.infiltration_air_changes_per_hour):
            trial_greenhouse_state.forced_air_changes_per_hour = airchanges_per_hr - self.greenhouse.infiltration_air_changes_per_hour
        else:
            trial_greenhouse_state.forced_air_changes_per_hour  = 0.0


        #now the humidity change is constant, or resolvable
        q,w = find_energy_and_humidity_forcing( self.greenhouse , trial_greenhouse_state, self.weather_state )
        # print( T, trial_greenhouse_state.forced_air_changes_per_hour  , q,w)
        # if( np.isclose(w , 0.0 , 0.00001 ) == False):
        #     q = 1000000 #this is a flag that the moisture is not steady state
        trial_greenhouse_state.Q_forcing = q
        trial_greenhouse_state.W_forcing = w
        # print( 'previous_greenhouse_state.T_topsoil '+ str(previous_greenhouse_state.T_topsoil) )
        # print( 'current_greenhouse_state.T_in '+ str(trial_greenhouse_state.T_in) )
        # deltatopsoil = top_soil_temperature_change(greenhouseA , trial_greenhouse_state,  weather_stateA, period_seconds)
        # topsoildeltaE = top_soil_energy_change( greenhouseA , trial_greenhouse_state,  weather_stateA)
        # print( 'top_soil_energy_change' + str(topsoildeltaE) )
        # print( 'delta.T_topsoil '+ str(deltatopsoil) )
        trial_greenhouse_state.T_topsoil = self.previous_greenhouse_state.T_topsoil + top_soil_temperature_change(self.greenhouse , trial_greenhouse_state, self.weather_state, self.period_seconds)

        self.greenhouse_state = trial_greenhouse_state
        # print(q)
        return q




def winter_solver_root_thermal_mass(greenhouseA , previous_greenhouse_state,  weather_stateA, period_seconds):
    #give a greenhouse, find the T_in that uses no heating or cooling. humidity_in fixed at RH 0.9. Aims for highest temperature possible
    global_RH_inside = 0.9

    greenhouse_state_orig = make_greenhouse_state( greenhouseA ,  weather_stateA)
    qfunctor = winter_solver_functor( greenhouseA , greenhouse_state_orig , previous_greenhouse_state, weather_stateA, global_RH_inside,  period_seconds )

    root = scipy.optimize.bisect(qfunctor, weather_stateA.AirTemp-10.0, 30.0 )

    greenhouse_state_optimum = qfunctor.greenhouse_state

    return greenhouse_state_optimum


def winter_solver_enumerate_thermal_mass(greenhouseA , previous_greenhouse_state,  weather_stateA, period_seconds):
    #give a greenhouse, find the T_in that uses no heating or cooling. humidity_in fixed at RH. Aims for highest temperature possible
    #takes into account the temperature of the soil from the previouse weather state
    global_RH_inside = 0.9

    greenhouse_state_orig = make_greenhouse_state( greenhouseA ,  weather_stateA)
    qfunctor = winter_solver_functor( greenhouseA , greenhouse_state_orig , previous_greenhouse_state, weather_stateA, global_RH_inside,  period_seconds )

    temperature_list = np.arange( weather_stateA.AirTemp , 40 , 0.1 )
    q_list = np.zeros( len(temperature_list) )
    trial_greenhouse_state_list = []
    for a in range( 0 , len(temperature_list) ):
        T = temperature_list[a]

        q = qfunctor(T)

        q_list[a] = q
        trial_greenhouse_state_list.append( copy.deepcopy(qfunctor.greenhouse_state) )

    greenhouse_state_optimum = greenhouse_state()
    bestabsq = 100000
    for a in range( 0 , len(temperature_list) ):
        if( abs(q_list[a]) < bestabsq):
            bestabsq = abs(q_list[a])
            # print( bestabsq)
            greenhouse_state_optimum = trial_greenhouse_state_list[a]

        # greenhouse_state_optimum.print()

    return greenhouse_state_optimum




    greenhouse_state_optimum = qfunctor.greenhouse_state


    return greenhouse_state_optimum













#these following solvers don't work very well. Included so you know what not to try













#these following solvers don't work very well. Included so you know what not to try
def brute_force_solver( greenhouseA , weather_stateA):
    #So you give a greenhouse and I'm going to enumerate all the possible
    #greenhouse states and then see which ones are allowed

    greenhouse_state_orig = make_greenhouse_state( greenhouseA ,  weather_stateA)
    print( "starting state was : ")
    greenhouse_state_orig.print()

    T_in_list = [ 15] #np.arange( greenhouseA.day_air_min  , greenhouseA.day_air_max , 5.0 )
    relative_humidity_list = np.array([0.1, 0.2 , 0.3 , 0.4 , 0.5 , 0.6 , 0.7 , 0.8 , 0.9 ])
    forced_air_changes_per_hour_list = np.arange( 40 , 150 , 5)

    Qtol = 50.0 #I want the energy for an acceptable solution to require at most +/- 50 watts
    Wmax = 0.0 #I will accept any solution where the humidity is changing at a rate less than this (kg water/sec)
        #the idea is it is easy to add humidity with misting, but much harder to remove it without large energy loss

    allowed_states = []
    best_state =greenhouse_state()
    bestabsq = 100000
    for t in T_in_list:
        for h in relative_humidity_list:
            for v in forced_air_changes_per_hour_list:
                greenhouse_stateB = greenhouse_state()
                greenhouse_stateB = greenhouse_state_orig
                greenhouse_stateB.T_in = t
                greenhouse_stateB.humidity_in = converting.relative_humidity_to_abs_humidity( t , h)
                greenhouse_stateB.forced_air_changes_per_hour = v

                q,w = find_energy_and_humidity_forcing( greenhouseA , greenhouse_stateB , weather_stateA)
                print( q,w)
                greenhouse_stateB.print()

                if( q<Qtol and q>-Qtol  and w<Wmax):
                    allowed_states.append(greenhouse_stateB)

                    if abs(q) < bestabsq:
                        bestabsq = abs(q)
                        best_state = greenhouse_stateB
                # f=0

    #find the "best" state, or one with smallest abs(q)
    print( "starting state was : ")
    greenhouse_state_orig.print()

    return best_state, bestabsq


#functor for optimizer
class objective_functor:
    def __init__(self , greenhouseA , greenhouse_stateA , weather_stateA):
        self.greenhouse = greenhouseA
        self.greenhouse_state = greenhouse_stateA
        self.weather_state = weather_stateA

    def __call__(self, xvec):
        #minimize this
        self.greenhouse_state.T_in = xvec[0]
        relative_humidity_inside = xvec[1]
        self.greenhouse_state.humidity_in = converting.relative_humidity_to_abs_humidity( xvec[0] , xvec[1] )
        self.greenhouse_state.forced_air_changes_per_hour = xvec[2]

        q,w = find_energy_and_humidity_forcing( self.greenhouse , self.greenhouse_state , self.weather_state)

        q_balance_penalty = abs(q)*1000.0
        w_balance_penalty = abs(w)*1000000.0

        T_in_kelvin = self.greenhouse_state.T_in  + 273

        # high_temp_penalty = 0
        # if( T_in_kelvin > self.greenhouse.day_air_max ):
        #     high_temp_penalty = (T_in_kelvin - (self.greenhouse.day_air_max + 273))*10
        #
        # relative_humidity_penalty = 0
        # relative_humidity_inside = converting.abs_humidity_to_relative_humidity(self.greenhouse_state.T_in, self.greenhouse_state.humidity_in)
        # if( relative_humidity_inside > 0.9 ):
        #     relative_humidity_penalty = (relative_humidity_inside - 0.9)*1000

        return 200.0 -T_in_kelvin + q_balance_penalty + w_balance_penalty


def simple_optimize_solver( greenhouseA , weather_stateA):
    #So you give a greenhouse and I'm going to optimize the objective function to find an allowed high temperature state

    greenhouse_state_orig = make_greenhouse_state( greenhouseA ,  weather_stateA)
    print( "starting state was : ")
    greenhouse_state_orig.print()

    mainfunctor = objective_functor( greenhouseA , greenhouse_state_orig , weather_stateA)
    xvec_start = [ 20 , 0.8 , 2]
    xbounds = [ (-10 , 30 ) , ( 0.2 , 0.9 ) , ( 0 , 200 ) ]

    res2 = scipy.optimize.fmin_slsqp(mainfunctor, xvec_start , bounds=xbounds)
    print( 'res2 = ' + str(res2) )
    greenhouse_state_optimum = make_greenhouse_state( greenhouseA ,  weather_stateA)
    greenhouse_state_optimum.T_in = res2[0]
    greenhouse_state_optimum.humidity_in = converting.relative_humidity_to_abs_humidity( res2[0] , res2[1] )
    greenhouse_state_optimum.forced_air_changes_per_hour = res2[2]
    print( find_energy_and_humidity_forcing( greenhouseA , greenhouse_state_optimum , weather_stateA) )



    print( "starting state was : ")
    greenhouse_state_orig.print()

    print( "optimum state was: ")
    greenhouse_state_optimum.print()

    return res2


def simple_multi_start_optimize_solver( greenhouseA , weather_stateA):
    #So you give a greenhouse and I'm going to optimize the objective function to find an allowed high temperature state

    greenhouse_state_orig = make_greenhouse_state( greenhouseA ,  weather_stateA)
    print( "starting state was : ")
    greenhouse_state_orig.print()

    mainfunctor = objective_functor( greenhouseA , greenhouse_state_orig , weather_stateA)
    xvec_start = [ 20 , 0.8 , 2]
    xbounds = [ (-10 , 30 ) , ( 0.2 , 0.9 ) , ( 0 , 200 ) ]

    n_starts = 100
    best_val = 1000000.0
    best_xvec = [ 0. , 0., 0. ]
    for a in range( 0 , n_starts):
        xvec_start= [ 0.0,0.0,0.0]
        xvec_start[0]= np.random.uniform( -10 ,30 ,1)
        xvec_start[1]= np.random.uniform( 0.2 ,0.9 ,1)
        xvec_start[2]= np.random.uniform( 0 , 200 ,1)

        x_sol = scipy.optimize.fmin_slsqp(mainfunctor, xvec_start , bounds=xbounds)
        val = mainfunctor(x_sol)
        if( val < best_val):
            best_val = val
            best_xvec = copy.deepcopy(x_sol)




    print( 'best val  = ' + str(best_val) )
    print( 'best xvec  = ' + str(best_xvec) )
    greenhouse_state_optimum = make_greenhouse_state( greenhouseA ,  weather_stateA)
    greenhouse_state_optimum.T_in = best_xvec[0]
    greenhouse_state_optimum.humidity_in = converting.relative_humidity_to_abs_humidity( best_xvec[0] , best_xvec[1] )
    greenhouse_state_optimum.forced_air_changes_per_hour = best_xvec[2]
    print( find_energy_and_humidity_forcing( greenhouseA , greenhouse_state_optimum , weather_stateA) )



    print( "starting state was : ")
    greenhouse_state_orig.print()

    print( "optimum state was: ")
    greenhouse_state_optimum.print()

    return greenhouse_state_optimum
