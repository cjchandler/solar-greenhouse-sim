#this is testing the greenhouse class

#setup a sample greenhouse.

#get some weather data, make a list of weather_state objects
from weather_data import *
import numpy as np
from panel import *
from greenhouse import *

path = '../test_solcast_data/'
filename = 'testmonth_Solcast_PT60M.csv'
f = load_solcast_historical_df( path , filename )
print(f)

weather_state_list = make_weather_state_list(f)

#define some panels
p_roof = panel()
p_wall = panel()
p_wall.theta = np.pi/2.0
p_wall.phi = np.pi

#simulate those panels for light transmission
p_roof_state_list = simulate_panel_for_panel_state_list( p_roof , weather_state_list)
p_wall_state_list = simulate_panel_for_panel_state_list( p_wall , weather_state_list)

#add panels to greenhouse object
greenhouseA = greenhouse()
greenhouseA.panel_list = [p_roof, p_wall ]

#make a state for greenhouse to record results in
greenhouse_stateA =make_greenhouse_state( greenhouseA , weather_state_list[66]  )
greenhouse_stateA.T_in  = 30
greenhouse_stateA.humidity_in = converting.relative_humidity_to_abs_humidity(greenhouse_stateA.T_in , 0.7 )
tt =converting.dewpoint_temperature( 40 , converting.relative_humidity_to_abs_humidity(40 , 0.5)  )
print( str(tt) + ' this should be close to 27.6')

#simulate greenhouse for each time period
#simulate_greenhouse()

 # sun up and shining bright at weather_state_list[66]
print( weather_state_list[66].RelativeHumidity)
#I'm going to test each Energy source and humidity source at this time period
#to test the calculations
weather_state66 =  weather_state_list[66]
greenhouse_state66 = make_greenhouse_state(greenhouseA , weather_state66)
#print out a little about the weather:
print( 'state66 t_sky is ' + str(greenhouse_state66.T_sky) )
print( 'state66 t_out is ' + str( greenhouse_state66.T_out ))
print( 'out humidity relative fraction' + str(weather_state66.RelativeHumidity*0.01) )
print( ' assume humidity in is ' + str(greenhouse_state66.humidity_in))
print( ' assume temperature in is ' + str(greenhouse_state66.T_in))

print( ' at these conditions, we have Q,W = '+ str( find_energy_and_humidity_forcing(greenhouseA , greenhouse_state66 , weather_state66)))

#is transpiration rate correct?
print( '------------------transpiration humidty changes -------------------------')
transpiration = transpiration_humidity_change_rate( greenhouseA , greenhouse_state66 , weather_state66)
print( 'transpiration in g / sec, crop area assumed 1  ' + str(transpiration)  )
es = 610.8*np.exp(17.27*6.2/(6.2+237.3) ) # in Pa
dem = (6.2+237.3)*(6.2+237.3)
delta = 4098000*es/dem
print( 'es in Pa is '  + str(es) )
print( 'delta in Pa/K is '  + str(delta) )

gamma = 0.66*( 1 + 0.00115 )*weather_state66.SurfacePressure*100.0
print( 'gamma constant is' + str(gamma) )

handtranspiration = (1.0/2260000.0)*( delta*greenhouse_state66.light_in_per_m2/( gamma + delta )   )
print( 'transpiration in kg / sec, crop area assumed 1  ' + str( handtranspiration)  )
print( 'transpiration in g / sec, crop area assumed 1  ' + str( 1000.0*handtranspiration)  )
#ok that's pretty close, so ok I think.


#is venting correct?
print( '------------------venting humidty changes -------------------------')
greenhouse_state66.forced_air_changes_per_hour = 10
greenhouseA.volume = 1
greenhouse_state66.T_in = 30
greenhouse_state66.humidity_in = converting.relative_humidity_to_abs_humidity(greenhouse_state66.T_in , 0.7 )
#try at inside temp 30c, 0.5 relative humidity
venthumidity = venting_humidity_change_rate( greenhouseA , greenhouse_state66 , weather_state66)
outsideabshumidity = converting.relative_humidity_to_abs_humidity(weather_state66.AirTemp , weather_state66.RelativeHumidity*0.01 )
print( 'relative humidity inside greenhouse  ' + str(0.7) )
print( 'abs humidity inside greenhouse g / m3  ' + str(greenhouse_state66.humidity_in) )
m3persec = greenhouseA.volume*( greenhouse_state66.forced_air_changes_per_hour + greenhouseA.infiltration_air_changes_per_hour)/(60*60)
print( 'm3 forced_air_changes_per_sec  ' + str(m3persec )  )
deltahumidity = ( outsideabshumidity - greenhouse_state66.humidity_in)
print( 'deltahumidity is g/m3  ' + str(deltahumidity) )
g_water_lost_in_venting_per_sec = m3persec*deltahumidity
print( ' g_water_lost_in_venting_per_sec by hand  '+ str(g_water_lost_in_venting_per_sec) )
print( ' g water lost per sec from program  '+ str(venthumidity))
# transpiration = transpiration_humidity_change_rate( greenhouseA , greenhouse_state66 , weather_state66)
# print( ' transpiration again at this temperature with water g/sec' + str(transpiration ) )
#ok, again close so no problem with equations. But it's clear that the venting has to be very high to get rid of all this moisture

print( '------------------condensation humidty changes -------------------------')
cond = condensation_humidity_change_rate(greenhouseA , greenhouse_state66 , weather_state66)
Tfilm = greenhouse_state66.panel_state_list[0].inside_film_temperature
print( '     greenhouse_state66.T_in' + str(greenhouse_state66.T_in))
print( '     greenhouse_state66.humidity_in ' + str(greenhouse_state66.humidity_in ))
print( '     greenhouse_state66. RELATIVE humidity_in ' + str(converting.abs_humidity_to_relative_humidity( greenhouse_state66.T_in , greenhouse_stateA.humidity_in ) ) )
dewpoint_inside = converting.dewpoint_temperature( greenhouse_state66.T_in , greenhouse_state66.humidity_in )
dewpoint_alt = 23.9
assert( np.isclose( dewpoint_inside , dewpoint_alt , 0.1 ))

#energy loss though panels is ?
deltaT = weather_state66.AirTemp - greenhouse_state66.T_in
u = greenhouse_state66.panel_state_list[0].U
elost = u*deltaT*2.0
print( '     elost through conduction is '+ str(elost) )
econduction = glazing_conduction_energy_change( greenhouseA , greenhouse_state66 , weather_state66)
print( '     elost from program '+ str( econduction) )

print( 'condensation rate in g/sec  ' + str(cond) )
condhand = elost/2260
print( 'condensation rate by hand in g/sec  ' + str(condhand) )



print( '------------------T_sky test--------------------------------------------')
ws_sky = weather_state()
ws_sky.CloudOpacity = 0.0
ws_sky.AirTemp = 5.0
ws_sky.RelativeHumidity = 5.0
calcskytemp = sky_temperature( ws_sky)
print( 'sky temp from program is ' + str(calcskytemp) )
print( 'sky temp from hand is ' + str(-10.9) )
assert( np.isclose( calcskytemp , -10.9 , 0.1))


print( '---------------glazing_radiation_energy_change test----------------------')
calcskytemp = sky_temperature( weather_state66)
stephanboltzmanconstant = 5.67e-8
energy = stephanboltzmanconstant*2.0*0.1*( (calcskytemp+273)**4 -(greenhouse_state66.T_in+273)**4 )
print( 'radiation glazing energy transfer is '+ str(energy) )
energycalc = glazing_radiation_energy_change(greenhouseA , greenhouse_state66 , weather_state66)
print( 'radiation glazing energy transfer from program is '+ str( energycalc  ) )
assert( np.isclose(energy, energycalc , 0.000001))


print( '---------------glazing conductive_energy change test----------------------')
deltaT = weather_state66.AirTemp - greenhouse_state66.T_in
energy = deltaT*(greenhouse_state66.panel_state_list[0].U + greenhouse_state66.panel_state_list[1].U)
energycalc = glazing_conduction_energy_change(greenhouseA , greenhouse_state66 , weather_state66)
assert( np.isclose(energy, energycalc , 0.000001))



# #print out a little about the weather:
# greenhouse_state66.panel_state_list[0].U = converting.r_imperial_to_U_si(0.3)
# greenhouse_state66.T_in = 22
# greenhouse_state66.forced_air_changes_per_hour = 0
# greenhouse_state66.humidity_in = 17.5
# print( '    state66 t_sky is ' + str(greenhouse_state66.T_sky) )
# print( '    state66 t_out is ' + str( greenhouse_state66.T_out ))
# print( '    out humidity relative fraction' + str(weather_state66.RelativeHumidity*0.01) )
# print( '    assume humidity in is ' + str(greenhouse_state66.humidity_in))
# print( '    humidity in relative is ' + str(converting.abs_humidity_to_relative_humidity(greenhouse_state66.T_in , greenhouse_state66.humidity_in)  )  )
# print( '    assume temperature in is ' + str(greenhouse_state66.T_in))
# print( '    assume forced_air_changes_per_hour  ' + str(greenhouse_state66.forced_air_changes_per_hour))
# q,w = find_energy_and_humidity_forcing(greenhouseA , greenhouse_state66 , weather_state66)
# print( '    at these conditions, we have Q in watts '+ str(q ) +' , W  in litres per hour = ' + str(w*60*60) )
# cond = condensation_humidity_change_rate(greenhouseA , greenhouse_state66 , weather_state66)
# print( 'condensation rate in liters per hour  ' + str(cond*60*60) )




#
# print( transpiration_humidity_change_rate( greenhouseA , greenhouse_stateA , weather_state_list[66] )*60*60 )
#
# print( condensation_humidity_change_rate( greenhouseA , greenhouse_stateA , weather_state_list[66] )*60*60 )
# print( venting_humidity_change_rate( greenhouseA , greenhouse_stateA , weather_state_list[66] )*60*60 )
#
# print( '    energy change from venting watts' + str(passive_venting_energy_change(greenhouseA , greenhouse_stateA , weather_state_list[66] ) ) )
#
# print( "  relative humidty outside " + str( weather_state_list[66].RelativeHumidity) )
# print( "  pressure outside " + str( weather_state_list[66].SurfacePressure) )
# print( "  air temp outside " + str( weather_state_list[66].AirTemp) )
# print( "  abs humidty outside " + str( converting.relative_humidity_to_abs_humidity( weather_state_list[66].AirTemp, 0.01*weather_state_list[66].RelativeHumidity) )  )
#
# simulate_greenhouse( greenhouseA , weather_state_list[66])
