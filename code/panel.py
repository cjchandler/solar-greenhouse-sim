#this is the panel class
import fresnel_multilayer_transmission as fresnel
import numpy as np
import conversion_funcs as converting
import pandas as pd


#this holds static values for panel
class panel:
    def __init__(self ):
        self.area = 1.0
        self.phi = 3.14 #this is pointing south
        self.theta = 0 #this is a flat horizontal panel
        self.n_index = 1.55 ##typical for polyethelyne film
        self.U_value_with_blanket = converting.r_imperial_to_U_si( 20.0 )
        self.U_value_without_blanket = converting.r_imperial_to_U_si( 2.0 )
        self.n_layers = 2
        self.mirror_fraction = 0.0 #fraction of hemisphere visible from panel that is mirror
        self.sky_fraction = 1.0 #fraction of hemisphere visible from panel that is sky. here 1.0 because a flat panel completly sees the sky
        #note that there isn't any way in compensate for hills or valley locations. instalation is assumed flat.
        self.ground_fraction = 0.0
        self.ground_albedo = 0.2 ###this is the albedo of the non-snow covered field around the panel.
        self.snow_albedo = 0.8 ###this is the albedo of the snow covered field around the panel.
        # It is assumed that the mirror is covered by snow when there is snow on the ground
        self.hemispherical_transmission = fresnel.hemispherical_4interface_transmission(self.n_index)

#this is keeping track of the panel properties that are weather dependent, like light in,
#or controlable by greenhouse operator like U value which can be modified by blankets
class panel_state:
    def __init__(self):
        self.sky_diffuse_incident = 0 #incident is watts light hitting the outside of glazing
        self.mirror_diffuse_incident = 0#diffuse light hitting the panel that came from mirror
        self.ground_diffuse_incident = 0#diffuse light hitting the panel that came from ground

        self.total_diffuse_incident=0
        self.total_diffuse_in=0 #total diffuse light in watts from all sources that is transmitted into the greenhouse

        self.mirror_direct_in = 0
        self.mirror_direct_incident = 0
        self.sun_direct_in= 0
        self.sun_direct_incident= 0

        self.total_direct_incident= 0
        self.total_direct_in= 0

        self.blanket_deployed = False
        self.films_inflated = True
        self.inside_film_temperature = -300
        self.inside_condensing = False
        self.e_value = 0.1
        self.U = 1000000.0 #this is filled when blanket_deployed etc

    def print( self ):
        print('    U panel is '+ str(self.U))


#this puts an opaque insulating blanked over the panel
def deploy_blanket( panelA , panel_stateA):
    panel_stateA.blanket_deployed = True
    panel_stateA.sky_diffuse_incident = 0
    panel_stateA.mirror_diffuse_incident = 0
    panel_stateA.ground_diffuse_incident = 0

    panel_stateA.total_diffuse_incident=0
    panel_stateA.total_diffuse_in=0

    panel_stateA.mirror_direct_in = 0
    panel_stateA.mirror_direct_incident = 0
    panel_stateA.sun_direct_in= 0
    panel_stateA.sun_direct_incident= 0

    panel_stateA.total_direct_incident= 0
    panel_stateA.total_direct_in= 0
    panel_stateA.e_value= 0
    panel_stateA.U = panelA.U_value_with_blanket

    return panel_stateA

#this removes an opaque insulating blanked over the panel
def remove_blanket( panelA , panel_stateA):
    panel_stateA.blanket_deployed = False
    panel_stateA.sky_diffuse_incident = 0
    panel_stateA.mirror_diffuse_incident = 0
    panel_stateA.ground_diffuse_incident = 0

    panel_stateA.total_diffuse_incident=0
    panel_stateA.total_diffuse_in=0

    panel_stateA.mirror_direct_in = 0
    panel_stateA.mirror_direct_incident = 0
    panel_stateA.sun_direct_in= 0
    panel_stateA.sun_direct_incident= 0

    panel_stateA.total_direct_incident= 0
    panel_stateA.total_direct_in= 0
    panel_stateA.e_value= 0.1
    panel_stateA.U = panelA.U_value_without_blanket

    return panel_stateA

#this takes a list of panel_state objects and converts it to a DataFrame
def panel_state_list_to_df( weather_dataA , panel_state_list):
    n_data = len( panel_state_list )
    print( 'panel state list has n elements = ' + str(n_data))
    sky_diffuse_incident = np.zeros(n_data)
    mirror_diffuse_incident = np.zeros(n_data)
    ground_diffuse_incident = np.zeros(n_data)
    total_diffuse_incident = np.zeros(n_data)
    total_diffuse_in = np.zeros(n_data)
    mirror_direct_in = np.zeros(n_data)
    mirror_direct_incident = np.zeros(n_data)
    sun_direct_in = np.zeros(n_data)
    sun_direct_incident = np.zeros(n_data)
    total_direct_incident = np.zeros(n_data)
    total_direct_in = np.zeros(n_data)
    blanket_deployed = np.zeros(n_data)
    films_inflated = np.zeros(n_data)
    inside_film_temperature = np.zeros(n_data)
    inside_condensing = np.zeros(n_data)

    for a in range( 0 , n_data):
        sky_diffuse_incident[a] = panel_state_list[a].sky_diffuse_incident
        mirror_diffuse_incident[a] = panel_state_list[a].mirror_diffuse_incident
        ground_diffuse_incident[a] = panel_state_list[a].ground_diffuse_incident
        total_diffuse_incident[a] = panel_state_list[a].total_diffuse_incident
        total_diffuse_in[a] = panel_state_list[a].total_diffuse_in
        mirror_direct_in[a] =panel_state_list[a].mirror_direct_in
        mirror_direct_incident[a] = panel_state_list[a].mirror_direct_incident
        sun_direct_in[a] = panel_state_list[a].sun_direct_in
        sun_direct_incident[a] =panel_state_list[a].sun_direct_incident
        total_direct_incident[a] = panel_state_list[a].total_direct_incident
        total_direct_in[a] =panel_state_list[a].total_direct_in
        blanket_deployed[a] = panel_state_list[a].blanket_deployed
        films_inflated[a] =panel_state_list[a].films_inflated
        inside_film_temperature[a] = panel_state_list[a].inside_film_temperature
        inside_condensing[a] = panel_state_list[a].inside_condensing


    dfout = pd.DataFrame(index=weather_dataA.index)
    dfout['sky_diffuse_incident'] = sky_diffuse_incident
    dfout['mirror_diffuse_incident'] = mirror_diffuse_incident
    dfout['ground_diffuse_incident'] = ground_diffuse_incident

    dfout['total_diffuse_incident'] = total_diffuse_incident
    dfout['total_diffuse_in'] = total_diffuse_in

    dfout['mirror_direct_in'] = mirror_direct_in
    dfout['mirror_direct_incident'] = mirror_direct_incident
    dfout['sun_direct_in'] = sun_direct_in
    dfout['sun_direct_incident'] = sun_direct_incident

    dfout['total_direct_incident'] = total_direct_incident
    dfout['total_direct_in'] = total_direct_in

    dfout['blanket_deployed'] = blanket_deployed
    dfout['films_inflated'] = films_inflated
    dfout['inside_film_temperature'] = inside_film_temperature
    dfout['inside_condensing'] = inside_condensing

    return dfout


def panel_film_temperature( panelA , T_in , T_out):
    #assume no blanket, when blanket is deployed there will be very little difference in air temp and film temp
    #this assumes U value of 8.35 for a still air layer and ignores the radiation heat transfer
    #if there is air movement inside the greenhouse T_film could be lower. Lower limit is T_out
    T_film = (panelA.U_value_without_blanket/8.3503)*(-T_in  +T_out) + T_in
    return T_film


#this calculates light incident and transmitted through the panel
#this also assumes 2 layers of film or glass. Changing that will
#break and assert. User will need to modify this for that case
def simulate_panel( panelA , panel_stateA , weather_stateA):

    #snow?
    #it is assumed that the panel itself is never covered in snow, that it slides
    # off or is removed before changing light transmission properties
    is_snow_covered = False

    if( weather_stateA.SnowDepth > 0.0 ):
        is_snow_covered = True


    ##how much diffuse light is incident?
    panel_stateA.sky_diffuse_incident = weather_stateA.Dhi*panelA.sky_fraction
    panel_stateA.mirror_diffuse_incident =weather_stateA.Dhi*panelA.mirror_fraction
    panel_stateA.ground_diffuse_incident = weather_stateA.Ghi*panelA.ground_albedo*panelA.ground_fraction

    #mirrors don't work if covered in snow
    if is_snow_covered:
        panel_stateA.mirror_diffuse_incident = weather_stateA.Ghi*panelA.snow_albedo*panelA.mirror_fraction
        panel_stateA.ground_diffuse_incident = weather_stateA.Ghi*panelA.snow_albedo*panelA.ground_fraction

    panel_stateA.total_diffuse_incident = (panel_stateA.sky_diffuse_incident+panel_stateA.mirror_diffuse_incident+panel_stateA.ground_diffuse_incident)*panelA.area
    panel_stateA.total_diffuse_in = panel_stateA.total_diffuse_incident*panelA.hemispherical_transmission




    #get sun direct
    sun_vector = converting.spherical_to_cartesian( 1.0 , weather_stateA.theta , weather_stateA.phi  )
    panel_vector = converting.spherical_to_cartesian( 1.0 , panelA.theta , panelA.phi  )

    dot = converting.dot_cartesian_vectors( panel_vector , sun_vector )
    if( dot < 0.0 ):
        dot = 0.0
    angle_between_panel_normal_and_sun = np.arccos(  dot)

    assert( panelA.n_layers == 2)
    panel_stateA.sun_direct_incident = panelA.area*weather_stateA.Dni*np.cos(angle_between_panel_normal_and_sun)
    panel_stateA.sun_direct_in = panel_stateA.sun_direct_incident*fresnel.T_quadruple_interface( angle_between_panel_normal_and_sun , 1.0, panelA.n_index )




    #get mirror direct, direct light coming to panel via the mirror on ground
    if is_snow_covered:
        panel_stateA.mirror_direct_incident = 0.0
        panel_stateA.mirror_direct_in = 0.0
    else:
        altitude_radians = 0.5*np.pi - weather_stateA.theta
        theta_mirror = weather_stateA.theta + 2.0*altitude_radians
        mirror_sun_vector =  converting.spherical_to_cartesian(1.0, theta_mirror  ,weather_stateA.phi )

        dot = converting.dot_cartesian_vectors( panel_vector , mirror_sun_vector )
        if( dot < 0.0 ):
            dot = 0.0
        angle_between_panel_normal_and_mirror_sun = np.arccos(  dot)

        assert( panelA.n_layers == 2)
        if( panelA.mirror_fraction > 0.0 ):
            panel_stateA.mirror_direct_incident = panelA.area*weather_stateA.Dni*np.cos(angle_between_panel_normal_and_mirror_sun)
            panel_stateA.mirror_direct_in = panel_stateA.sun_direct_incident*fresnel.T_quadruple_interface( angle_between_panel_normal_and_mirror_sun , 1.0, panelA.n_index )

    panel_stateA.total_direct_incident = panel_stateA.mirror_direct_incident + panel_stateA.sun_direct_incident
    panel_stateA.total_direct_in = panel_stateA.mirror_direct_in + panel_stateA.sun_direct_in


    return panel_stateA





#these fucntions are here for convenience but aren't really used in the simulation workflow
def simulate_panel_for_panel_state_list( panelA , weather_state_list):
    panel_state_list = []
    n_data = len(weather_state_list )
    for a in range( 0 , n_data):
        ps= panel_state()
        ps = remove_blanket(panelA, ps)
        ps = simulate_panel( panelA , ps , weather_state_list[a])
        panel_state_list.append(ps)
    return panel_state_list

def simulate_panel_for_panel_df( panelA , weather_state_list, weather_df ):
    panel_state_list = simulate_panel_for_panel_state_list( panelA , weather_state_list)

    pr = panel_state_list_to_df(weather_df,panel_state_list )
    return pr
























def simulate_panel_old( panelA , weather_dataA ):
    n_data = len(weather_dataA['phi'].to_numpy())


    #I use the colums: SnowDepth, Dhi, Ghi, theta , phi, Dni
    #I make numpy vectors here because acessing data in pandas
    #is super slow. This is way faster and makes the code a bit tidier
    #too.
    SnowDepth = weather_dataA['SnowDepth'].to_numpy()
    Dhi = weather_dataA['Dhi'].to_numpy()
    Ghi = weather_dataA['Ghi'].to_numpy()
    theta = weather_dataA['theta'].to_numpy()
    phi = weather_dataA['phi'].to_numpy()
    Dni = weather_dataA['Dni'].to_numpy()

    ##these are the output columns
    sky_diffuse_incident = np.zeros( n_data)
    mirror_diffuse_incident = np.zeros( n_data)
    ground_diffuse_incident = np.zeros( n_data)

    total_diffuse_in = np.zeros( n_data)
    total_diffuse_incident = np.zeros( n_data)

    sun_direct_in =  np.zeros( n_data)
    sun_direct_incident =  np.zeros( n_data)

    mirror_direct_in =  np.zeros( n_data)
    mirror_direct_incident =  np.zeros( n_data)

    total_direct_in =  np.zeros( n_data)
    total_direct_incident =  np.zeros( n_data)


    #loop over all time periods
    for a in range( 0 , n_data):
        #snow?
        is_snow_covered = False

        if( SnowDepth[a] > 0.0 ):
            is_snow_covered = True


        ##how much diffuse light is incident?
        sky_diffuse_incident[a] = Dhi[a]*panelA.sky_fraction
        mirror_diffuse_incident[a] =Dhi[a]*panelA.mirror_fraction
        ground_diffuse_incident[a] = Ghi[a]*panelA.ground_albedo*panelA.ground_fraction

        #mirrors don't work if covered in snow
        if is_snow_covered:
            mirror_diffuse_incident[a] = Ghi[a]*panelA.snow_albedo*panelA.mirror_fraction
            ground_diffuse_incident[a] = Ghi[a]*panelA.snow_albedo*panelA.ground_fraction

        total_diffuse_incident[a] = (sky_diffuse_incident[a]+mirror_diffuse_incident[a]+ground_diffuse_incident[a])*panelA.area
        total_diffuse_in[a] = total_diffuse_incident[a]*panelA.hemispherical_transmission




        #get sun direct
        sun_vector = converting.spherical_to_cartesian( 1.0 , theta[a] , phi[a]  )
        panel_vector = converting.spherical_to_cartesian( 1.0 , panelA.theta , panelA.phi  )

        dot = converting.dot_cartesian_vectors( panel_vector , sun_vector )
        if( dot < 0.0 ):
            dot = 0.0
        angle_between_panel_normal_and_sun = np.arccos(  dot)

        assert( panelA.n_layers == 2)
        sun_direct_incident[a] = panelA.area*Dni[a]*np.cos(angle_between_panel_normal_and_sun)
        sun_direct_in[a] = sun_direct_incident[a]*fresnel.T_quadruple_interface( angle_between_panel_normal_and_sun , 1.0, panelA.n_index )




        #get mirror direct, direct light coming to panel via the mirror on ground
        if is_snow_covered:
            mirror_direct_incident[a] = 0.0
            mirror_direct_in[a] = 0.0
        else:
            altitude_radians = 0.5*np.pi - theta[a]
            theta_mirror = theta[a] + 2.0*altitude_radians
            mirror_sun_vector =  converting.spherical_to_cartesian(1.0, theta_mirror  ,phi[a] )

            dot = converting.dot_cartesian_vectors( panel_vector , mirror_sun_vector )
            if( dot < 0.0 ):
                dot = 0.0
            angle_between_panel_normal_and_mirror_sun = np.arccos(  dot)

            assert( panelA.n_layers == 2)
            if( panelA.mirror_fraction > 0.0 ):
                mirror_direct_incident[a] = panelA.area*Dni[a]*np.cos(angle_between_panel_normal_and_mirror_sun)
                mirror_direct_in[a] = sun_direct_incident[a]*fresnel.T_quadruple_interface( angle_between_panel_normal_and_mirror_sun , 1.0, panelA.n_index )

        total_direct_incident[a] = mirror_direct_incident[a] + sun_direct_incident[a]
        total_direct_in[a] = mirror_direct_in[a] + sun_direct_in[a]

    dfout = pd.DataFrame(index=weather_dataA.index)
    dfout['sky_diffuse_incident'] = sky_diffuse_incident
    dfout['mirror_diffuse_incident'] = mirror_diffuse_incident
    dfout['ground_diffuse_incident'] = ground_diffuse_incident

    dfout['total_diffuse_incident'] = total_diffuse_incident
    dfout['total_diffuse_in'] = total_diffuse_in

    dfout['mirror_direct_in'] = mirror_direct_in
    dfout['mirror_direct_incident'] = mirror_direct_incident
    dfout['sun_direct_in'] = sun_direct_in
    dfout['sun_direct_incident'] = sun_direct_incident

    dfout['total_direct_incident'] = total_direct_incident
    dfout['total_direct_in'] = total_direct_in

    return dfout
