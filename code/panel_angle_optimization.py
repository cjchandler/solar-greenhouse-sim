#get some weather data, make a list of weather_state objects
from weather_data import *
import numpy as np
from panel import *
from greenhouse import *
from greenhouse_solver import *
import conversion_funcs as converting
import pandas as pd
import matplotlib.pyplot as plt

path = '~/datalogging/solcast/'
filename = '43.913601_-66.070782_Solcast_PT60M.csv'
# path = '../test_solcast_data/'
# filename = 'testmonth_Solcast_PT60M.csv'
f = load_solcast_historical_df( path , filename )
print(f)

# df_reindexed = f.reindex(pd.date_range(start=f.index.min(),end=f.index.max(),freq='30T'))
# f = df_reindexed.interpolate(method='linear', limit_direction='forward', axis=0)
# print(  f )


weather_state_list = make_weather_state_list(f)

#define some panels
p_roof = panel()
p_wall = panel()
p_wall.theta = np.pi/2.0
p_wall.phi = np.pi
p_wall.sky_fraction = 0.5
p_wall.ground_fraction = 0.5

p_roof.n_index = 1.4
p_wall.n_index = 1.4


panel_list = []
anglesDeg = [ 0 , 10 , 20 , 25,  30 , 35 , 40 ,45, 50 ,55, 60 ,65, 70 ,75, 80 , 90 ]
for a in anglesDeg:
    rad = np.deg2rad(a)
    ptemp = panel()
    ptemp.theta = rad
    ptemp.phi = np.pi*0.5
    ptemp.albedo = 0.9
    ptemp.sky_fraction = np.cos(rad)
    ptemp.ground_fraction = np.sin(rad)
    panel_list.append(ptemp)

p_wall_with_flat_mirror = panel()
p_wall_with_flat_mirror.theta = -np.pi/2.0
p_wall_with_flat_mirror.phi = np.pi
p_wall_with_flat_mirror.sky_fraction = 0.5
p_wall_with_flat_mirror.ground_fraction = 0.07
p_wall_with_flat_mirror.mirror_fraction = 0.43

p_wall.albedo = 0.9

# panel_list.append(p_wall_with_flat_mirror)
# panel_list.append(p_wall)


f['cm_of_snow_depth'] = f['SnowDepth'].to_numpy()*10.0
f['inches_of_snow_depth'] = f['cm_of_snow_depth'].to_numpy()/2.54
#using townsend and powers equation for anual snow loss
#annual snow fall- count only upsteps in inched of snow depth.
inches_of_snow_depth_list = f['cm_of_snow_depth'].to_numpy()/2.54
inches_of_snowfall = 0
for a in range( 0 , len(inches_of_snow_depth_list)-1):
    if( inches_of_snow_depth_list[a+1] > inches_of_snow_depth_list[a] ):
        inches_of_snowfall = inches_of_snowfall + inches_of_snow_depth_list[a+1] - inches_of_snow_depth_list[a]


# https://www.currentresults.com/Weather/Canada/Nova-Scotia/snowfall-annual-average.php
anual_snow_inches = 81.0
print( 'annual snow inches - ' + str(anual_snow_inches) )
print( 'annual snow inches solcast calc- ' + str(inches_of_snowfall/12.0) )
def townsend_snow_reduction( snow_inches , theta_rad ):
    percent_loss = 0.1*snow_inches*np.cos(theta_rad)*np.cos(theta_rad)
    return percent_loss

#simulate those panels for light transmission
#this assumes panels are always cleared from snow
print( " mean Ghi is " + str(np.mean( f['Ghi'].to_numpy() ) )  )


meanSnowCorrectedWatts = []
meanWatts = []
ff = pd.DataFrame(index=f.index)

for panelB in panel_list:
    p_state_list = simulate_panel_for_panel_state_list( panelB , weather_state_list)

    temp = []
    for a in range( len(p_state_list) ):
        temp.append( p_state_list[a].total_direct_incident + p_state_list[a].total_diffuse_incident  )

    deg = str( np.rad2deg(panelB.theta ) )
    ff['E_incident_panel_'+deg ]= temp
    meanwatts = np.mean( temp )
    print( 'mean E_incident_panel_'+deg+ ' = ' +  str(np.mean( temp ) )  )
    reduction = 0
    if( float(deg) <= 40.0):
        reduction = townsend_snow_reduction(anual_snow_inches , panelB.theta  )
        print( " mean with anual snow loss factored in : " + str( meanwatts - meanwatts*reduction*0.01 ))
    if( float(deg) >40.0  and float(deg) < 60.0 ):
        reduction = 0.5*townsend_snow_reduction(anual_snow_inches , panelB.theta  )
        print( " mean with anual snow loss factored in : " + str( meanwatts - meanwatts*reduction*0.01 ))

    meanSnowCorrectedWatts.append( meanwatts - meanwatts*reduction*0.01  )
    meanWatts.append( meanwatts   )

g = pd.DataFrame(meanSnowCorrectedWatts, index =anglesDeg, columns =['mean_watts_snow_corrected'])
g[ 'mean_watts_no_snow_correction'] = meanWatts




monthf = ff.resample("M").mean()
print(monthf)

monthff = ff.groupby(ff.index.month).mean()
print(monthff )
# monthff.to_csv( 'monthly_angle_dependent_means.csv')
maxValueIndexObj = monthff.idxmax(axis=1)
print( "best angle for each month")
print( maxValueIndexObj)


#snow depth is in cm of water equivilent. So times 10 is cm of snow
g.plot(grid=True )
plt.show()
