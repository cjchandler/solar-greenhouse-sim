#this is for testing the weather_data.py file
from weather_data import *
from panel import *
import conversion_funcs as converting

path = '../test_solcast_data/'
filename = 'panel_unit_test_data.csv'
w = load_solcast_historical_df( path , filename )
print(w)
weather_state_list = make_weather_state_list(w)

p = panel()
panel_state_list = []
n_data = len(w['AirTemp'].to_numpy())
for a in range( 0 , n_data):
    ps= panel_state()
    ps = simulate_panel( p , ps , weather_state_list[a])
    panel_state_list.append(ps)
pr = panel_state_list_to_df( w,panel_state_list )

print(pr[ ['total_direct_incident' ,'total_diffuse_incident'] ])

total_diffuse_incident = pr['total_diffuse_incident'].to_numpy()
total_direct_incident = pr['total_direct_incident'].to_numpy()
assert( total_diffuse_incident[0] == 100)
assert( total_diffuse_incident[1] == 5)
assert( total_direct_incident[1] == 100)
assert( np.isclose(total_direct_incident[2] , 50.0 , 0.000001 ) )


#check panel area works
p = panel()
p.area = 2.0
pr = simulate_panel_for_panel_df( p , weather_state_list, w)
total_diffuse_incident = pr['total_diffuse_incident'].to_numpy()
total_direct_incident = pr['total_direct_incident'].to_numpy()
assert( total_direct_incident[1] == 200)

print( 'panel at 45 degrees')
p = panel()
p.theta = np.pi/4.0
p.phi = np.pi
p.sky_fraction = 0.75
p.mirror_fraction = 0.0
p.ground_fraction = 0.0
pr = simulate_panel_for_panel_df( p , weather_state_list, w)
total_diffuse_incident = pr['total_diffuse_incident'].to_numpy()
total_direct_incident = pr['total_direct_incident'].to_numpy()
print(pr[ ['total_direct_incident' ,'total_diffuse_incident' , 'sun_direct_incident' , 'mirror_direct_incident'] ])
temp = 100.0*np.cos( np.pi/4.0 )
assert( np.isclose(total_direct_incident[1] , temp , 0.000001 ) )
temp = 100.0*np.cos( np.deg2rad(15) )
assert( np.isclose(total_direct_incident[2] , temp , 0.000001 ) )
assert( np.isclose(total_diffuse_incident[1] , 5*0.75 , 0.000001 ) )

print( 'panel at 45 degrees with and withouth snow')
p = panel()
p.theta = np.pi/4.0
p.phi = np.pi
p.sky_fraction = 0.75
p.mirror_fraction = 0.2
p.ground_fraction = 0.05
p.snow_albedo = 0.8
p.ground_albedo = 0.2
pr = simulate_panel_for_panel_df( p , weather_state_list, w)
total_diffuse_incident = pr['total_diffuse_incident'].to_numpy()
total_direct_incident = pr['total_direct_incident'].to_numpy()
print(pr[ ['total_direct_incident' ,'total_diffuse_incident' , 'sun_direct_incident' , 'mirror_direct_incident'] ])
assert( np.isclose(total_diffuse_incident[3] , 14.75 , 0.000001 ) )
assert( np.isclose(total_diffuse_incident[4] , 3.75 + 0.05*0.2*55 + 0.2*1.0*5 , 0.000001 ) )
temp = 100.0*np.cos( np.deg2rad(15) )
assert( np.isclose(total_direct_incident[3] , temp , 0.000001 ) )
temp = 100.0*np.cos( np.deg2rad(15) )
temp2 = 100*np.sin( np.deg2rad(15) )
#the asumption with the mirror is that it's large enough so that it reflects light over the whole panel for all sun angles
assert( np.isclose(total_direct_incident[4] , temp + temp2, 0.000001 ) )


print( 'area effects panel at 45 degrees with and withouth snow')
p = panel()
p.theta = np.pi/4.0
p.phi = np.pi
p.area = 2.0
p.sky_fraction = 0.75
p.mirror_fraction = 0.2
p.ground_fraction = 0.05
p.snow_albedo = 0.8
p.ground_albedo = 0.2
pr = simulate_panel_for_panel_df( p , weather_state_list, w)
total_diffuse_incident = pr['total_diffuse_incident'].to_numpy()
total_direct_incident = pr['total_direct_incident'].to_numpy()
print(pr[ ['total_direct_incident' ,'total_diffuse_incident' , 'sun_direct_incident' , 'mirror_direct_incident'] ])
assert( np.isclose(total_diffuse_incident[3] , 14.75*2 , 0.000001 ) )
assert( np.isclose(total_diffuse_incident[4] , 2.0*(3.75 + 0.05*0.2*55 + 0.2*1.0*5) , 0.000001 ) )

print( panel_film_temperature( p , 30 , 0  ) )

print( "passed all tests")
