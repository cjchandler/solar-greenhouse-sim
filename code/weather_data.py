
import conversion_funcs as converting
import pandas as pd

import numpy as np





def load_solcast_historical_df( path , filestring ):
    f = pd.read_csv(path + filestring, index_col='PeriodStart', parse_dates=True, sep=',')
    #ok that's good , but I need the sun azimuth and zeineth as radians

    z = f['Zenith'].to_numpy()
    a = f['Azimuth'].to_numpy()
    f['theta'] = np.deg2rad( z)
    f['phi'] = np.deg2rad( a)

    # print( f)

    return f

class weather_state: #this object stores weather data for one time period

    def __init__(self ):
        self.date = ' '

        self.AirTemp = 0
        self.Azimuth = 0
        self.CloudOpacity = 0
        self.DewpointTemp = 0
        self.Dhi = 0
        self.Dni = 0
        self.Ebh = 0
        self.Ghi=0
        self.PrecipitableWater=0
        self.RelativeHumidity=0
        self.SnowDepth=0
        self.SurfacePressure=0
        self.WindDirection10m=0
        self.WindSpeed10m=0
        self.Zenith=0
        self.theta = 0
        self.phi = 0

def make_weather_state_list( weather_df):
    #this takes a pandas data frame of weather data and converts into a list of weather state objects
    date = weather_df.index.to_numpy()
    AirTemp = weather_df['AirTemp'].to_numpy()
    Azimuth = weather_df['Azimuth'].to_numpy()
    CloudOpacity = weather_df['CloudOpacity'].to_numpy()
    DewpointTemp = weather_df['DewpointTemp'].to_numpy()
    Dhi = weather_df['Dhi'].to_numpy()
    Dni = weather_df['Dni'].to_numpy()
    Ebh = weather_df['Ebh'].to_numpy()
    Ghi=weather_df['Ghi'].to_numpy()
    PrecipitableWater=weather_df['PrecipitableWater'].to_numpy()
    RelativeHumidity=weather_df['RelativeHumidity'].to_numpy()
    SnowDepth=weather_df['SnowDepth'].to_numpy()
    SurfacePressure=weather_df['SurfacePressure'].to_numpy()
    WindDirection10m=weather_df['WindDirection10m'].to_numpy()
    WindSpeed10m=weather_df['WindSpeed10m'].to_numpy()
    Zenith=weather_df['Zenith'].to_numpy()
    theta = weather_df['theta'].to_numpy()
    phi = weather_df['phi'].to_numpy()

    n_data = len(AirTemp)
    out = []
    for a in range( 0 , n_data):
        w = weather_state()
        w.date = date[a]
        w.AirTemp = AirTemp[a]
        w.Azimuth = Azimuth[a]
        w.CloudOpacity = CloudOpacity[a]
        w.DewpointTemp = DewpointTemp[a]
        w.Dhi = Dhi[a]
        w.Dni = Dni[a]
        w.Ebh = Ebh[a]
        w.Ghi=Ghi[a]
        w.PrecipitableWater=PrecipitableWater[a]
        w.RelativeHumidity=RelativeHumidity[a]
        w.SnowDepth=SnowDepth[a]
        w.SurfacePressure=SurfacePressure[a]
        w.WindDirection10m=WindDirection10m[a]
        w.WindSpeed10m=WindSpeed10m[a]
        w.Zenith=Zenith[a]
        w.theta = theta[a]
        w.phi = phi[a]
        out.append(w)
    return out
