
\documentclass{article}
\usepackage{hyperref}
\usepackage{graphicx}
%\graphicspath{{../pdf/}{D:\ImagesforProjectLatex}}
\begin{document}


\title{Solar Greenhouse Sim}
\author{Carl Chandler }


\maketitle




\begin{abstract}
This is a simulation of a greenhouse based on historical solar irradiance and weather data. This will give a good first guess at the performance of a greenhouse in a given climate. Specific greenhouse glazing geometries can be constructed and simulated over the historical time period. Light transmission is based on Fresnel equations. Options for mirrors to increase light are provided. Heat loss includes options for nighttime blankets to cover the glazing, soil specific heat capacity. Humidity loss and gains from transpiration, condensation and air exchange are included.   

Solar Greenhouse Sim is supported by solar irradiance data from Solcast. \url{http://solcast.com}

  
\end{abstract}

\section{License}
Solar Greenhouse Sim is released under GNU General Public License v3.0. Briefly, use and copying is free for personal and commercial use, but the source code must be made available and license maintained.

\section{Workflow Overview}
The first step is to load a dataset for weather data as a pandas dataframe \verb|weather_df|. This is in the same format used by solcast \verb|.csv| data files, however a little bit of handling is needed to change units and use the time as index. UTC time is used throughout to avoid problems with time zones and daylight savings time. This data frame can be saved for future use as a .csv file. Each row is converted into a \verb|weather_state| object which has all the weather data for a given time period. 

The second step is to create the \verb|panel| objects for the greenhouse structure. A full 3D model is not created, each panel is considered individually as a flat area of greenhouse glazing and there is no self shading consideration. For simple geometries this should not be too limiting. 

A \verb|greenhouse| object is then initialized, and the \verb|panels| are added to it. The \verb|panel| and \verb|greenhouse| objects store immutable parameters like size, orientation. 

To run a simulation, the function \verb|make_greenhouse_state| is used to make \verb|greenhouse_state| object, which contains \verb|panel_state| objects. The state objects store results of the simulation. 

Solving the energy balance and humidity balance equations is non-trivial since it involves a system of non-linear equations. Various solver functions are included, which make different assumptions. Users may need to write their own solver for different design criteria. The solver takes a \verb|greenhouse| structure, \verb|weather_state|, and possible period length and a previous \verb|greenhouse_state| and returns a simulated \verb|greenhouse_state|. 

The output can be analyzed by converting a list of \verb|greenhouse_state| into a pandas dataframe. The \verb|df.plot()| function can quickly plot results of interest. 



\section{ Mirrors}
A unique aspect of solar greenhouse sim is the ability to use mirrors to increase light inside greenhouses. For simplicity (and keeping in mind the practical feasibility) mirrors are assumed to lay flat on the ground on the south side of the greenhouse. The albedo of ground in the field of view of greenhouse panels is also adjustable when initializing panels. 


\section{Coordinate system} 
Radians are used as angular measurement throughout and $\phi$ and $\theta$ are used as orientation angles. $\theta$ is the angle between vertical and the vector, $phi = 0 $ is north, west is $\phi = \pi / 2$ , south is $\phi = \pi 2$, east is $\phi = -\pi / 2$. 

\section{Panels}
The first basic structure is the panel class. This represents a flat area of glazing with a given orientation, index of refraction, number of films. It also is given a fixed view fraction of sky, mirror, or ground. There isn't any way in compensate for hills or valley locations. Installation is assumed flat with unobstructed horizon.

The light in is assumed to be completely captured by plants. This may be a poor assumption for low crops, but for high wire tomatoes, cucumbers, and other tall plants it should be reasonably accurate.

Light transmission is assumed to be governed entirely by Fresnel's equations. Films only reflect and transmit light, which isn't a bad assumption for thin films. Multiple reflections are included 2 layer films, single layer film panels are not currently supported. 

Heat loss through glazing is controlled by the thermal conductivity $U$ and IR radiation emissivity $e$. The $U$ value includes conduction and convective effects, but not radiation effects, and thus is smaller than equivalent building code empirical R-values. It is assumed that all thermal radiation is emitted by the panel material itself, that the film itself or a layer of condensation absorbs all IR emitted from the greenhouse interior and reemits it.

A thermal blanket option in included which can modify the panel thermal properties at night. This is assumed to be opaque to all light, with a new $U$ value that supplants the daytime panel $U$.  

\subsection{Transmission Physics}
Most important transmission characteristics of a glazing can be predicted by the index of refraction. First consider direct light, light incident to the normal surface vector ( or "head on"). Here Fresnell equations are simplified to:
\begin{equation}
T = \frac{4 n_1}{( n_1 + n_2)^2}
\end{equation}  
$n_1$ is the index of refraction of the first medium, $n_2$ is the index of refraction in the second medium. 
\begin{equation}
T+ R = 1
\end{equation}
Assume that any light not transmitted is reflected. A film or pane has two interfaces, light going into the glazing, and light going out of the glazing. There are potentially light paths that bounce multiple times between the two interfaces before finally being transmitted. The correction accounting for these paths (summing the geometric series) is: 
\begin{equation}
T_{2 interfaces} = \frac{T^2}{ 1 - R^2}
\end{equation}
Likewise, the calculation for double films can be done by repeating the process. 
\begin{equation}
T_{4 interfaces} = \frac{T_{2 interfaces}^2 }{ 1 - (1-T_{2 interfaces})^2   }
\end{equation}
%%march 24 2020 papers notes, /farm/greenhouseSolar/analytical_multi_film_solar_transmission.py

If the light is not normal to the surface, the full Fresnel equations must be used, with the path angle changing in accordance to Snell's law:
\begin{equation}
n_i sin(\theta_i) = n_t sin(\theta_t)
\end{equation}
All angles measured from the surface normal, $\theta = 0$ reduces the full Fresnel equations to previous calculations. The s and p polarizations are considered separately, but averaged for general non-polarized sunlight. 
\begin{equation}
R_s = (\frac{ n_i cos(\theta_i) - n_t cos(\theta_t) }{n_i cos(\theta_i) + n_t cos(\theta_t)})^2
\end{equation} 
\begin{equation}
R_p = (\frac{ n_i cos(\theta_t) - n_t cos(\theta_i) }{n_i cos(\theta_t) + n_t cos(\theta_i)})^2
\end{equation} 
\begin{equation}
R_{fresnel}(\theta) = \frac{1}{2} ( R_s + R_p)
\end{equation}
\begin{equation}
T_{fresnel}(\theta) = 1 - R(\theta)
\end{equation}

The Fresnel equations can be included with multiple layer corrections. 

\begin{equation}
T_{direct}(\theta) = M_r T_{fresnel1}(\theta)^2 T_{fresnel2}(\theta)^2 
\end{equation}
$T_{fresnel1}$ is transmission through one interface of the first film, $ T_{fresnel2}$ is transmission through one interface of the second film. 
\begin{equation}
M_r = \frac{T_{4 interfaces}}{T^4}
\end{equation}
$M_r$ is a correction factor for multiple reflections, generally small $\approx 1.02$. 
\begin{equation}
T_{diffuse}(\theta) = \int_0^{2\pi} d\phi\int_0^{\pi/2} d\theta M_r T_{fresnel1}(\theta)^2 T_{fresnel2}(\theta)^2 cos(\theta)sin(\theta)
\end{equation}   
 
The index of refraction does seem to control all important parameters of greenhouse glazing optical properties, see table \ref{tab:my-table}. The only problem seems to be the diffuse poly, which was measured by Hemming \cite{hemming2008} to have a much lower transmission than calcualted here and in Tantau 2012 \cite{tantau2012}. It's possible the modern diffuse poly has improved optically since 2008.

\begin{table}[]
\begin{tabular}{llllllll}
\multicolumn{1}{l}{material} & n    & $T_{direct}$ &  $T_{diffuse}$ & $2T_{direct}$ & $2T_{diffuse}$ & $Hemming_{direct}$ & $Hemming_{diffuse}$ \\ 
standard glass                 & 1.5  & 0.9230               & 0.8368                & 0.8571               & 0.7277                &       0.8938                       &    0.8219                          \\
clear poly-EVA                 & 1.55 & 0.9110               & 0.8241                & 0.8367               & 0.7077                &           0.8918                   &   0.7917                           \\
diffuse poly-EVA               & 1.55 & 0.9110               & 0.8241                & 0.8367 
&  0.7077                     &    0.8766                          &   0.7346                           \\
ETFE                           & 1.4  & 0.9459               & 0.8632                & 0.8974               & 0.7704                &     0.9362                         &  0.8579                           
\end{tabular}
\caption{Calculated transmission with experimental results from Hemming 2008. $2T$ is for a double film layer. }
\label{tab:my-table}
\end{table}


\section{Greenhouse}

The greenhouse structure is composed of a list of panels, and other global parameters like floor area and volume. 

The greenhouse state has most of the final parameters of interest like inside temperature, inside humidity, total light in per unit floor area. The temperature of the top soil ( or other thermal mass)
is included. Any state is allowed, but the \verb|Q_forcing| and \verb|W_forcing| values need to be $0$ for a steady state. 

All light transmitted in is assumed to be available by crops. For some crop geometry this may be a poor assumption and a significant portion absorbed by walls or reflected out of the greenhouse. 

Humidity balance, or $W$ is controlled by transpiration, condensation on panels, and venting. The transpiration calculations are somewhat simplistic and to not explicitly take into account temperature but should be a good starting point. Condensation panels assumes a still air layer next to them, condensation may be higher in a ventilated situation, or with inside fans. 

Energy balance is controlled by panel $U$ conductive and convective losses, radiation losses which depends on sky temperature, air-topsoil heat transfer, topsoil-deep soil energy transfer, venting, and solar heating. 

Venting energy loss includes the specific heat of air and the latent heat loss.  For both the condensation and latent heat loss, the simple latent heat of vaporization is used and no attempt to correct for cooling or heating water in it's liquid or gas state. 



\section{Solvers}

The solvers are given a weather state, greenhouse, and previous greenhouse state. The goal is then to find a greenhouse state where \verb|W_forcing| $= 0$ and \verb|Q_forcing| $= 0$. For a given inside temperature, the winter solver used the $\sum W = 0 $ to find the appropriate venting rate, then Q. A root finding procedure then finds the inside temperature for which $Q = 0$. 

If the conductivity between air-topsoil is high, the time step might need to be smaller than 1 hour. This can be done by interpolating the weather data with Pandas.  


\section{ Weather data sets} 
This program has been tested with data from Solcast, specifically their 12 year hourly historical data. Many thanks to them for providing some free test data to use with this package. The free canadian CWEEDS data set provides the same information, but when compared with the solcast data for Yarmouth, NS the CWEEDS data showed much higher solar irradiance estimates. The solcast data agreed with our local PAR measurements (apogee sq-120) and a conservative estimate of winter irradiance is better for feasibility studies so we do not recommend using CWEEDS data.
















\bibliographystyle{plain}
\bibliography{testRefs}

\end{document}
